<?php
if (!isset($attributes['id'])){
	$attributes['id'] = 'modalTablaBusqueda';
}

if (!isset($attributes['idTabla'])){
	$attributes['idTabla'] = 'tabla';
}

if (!isset($attributes['titulo'])){
	$attributes['titulo'] = 'Buscar';
}

if (!is_array($value)){
	$value = [];
}
?>
<div id="{{ $attributes['id'] }}" class="modal modal-busqueda fade" tabindex="-1" role="dialog">
	<div class="modal-dialog container">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">{{ $attributes['titulo'] }}</h4>
			</div>
			<div class="modal-body">
				<table id="{{ $attributes['idTabla'] }}" class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							@foreach($value as $columna => $ancho)
							<th style="width: {{ $ancho }}%;">{{ $columna }}</th>
							@endforeach
						</tr>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn blue" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>