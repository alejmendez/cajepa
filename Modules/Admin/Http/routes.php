<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix')], function () {
	//Route::get('/', 'escritorioController@getIndex');

	Route::get('/', 'escritorioController@getIndex');

	Route::controllers([
		'menu'			=> 'menuController',
		'perfiles'		=> 'perfilesController',
		'usuarios'		=> 'usuariosController',

		'login'			=> 'loginController',
		'cambiarclave'	=> 'cambiarclaveController',
		'perfil'   		=> 'UsuarioPerfilController',
		'prueba'		=> 'pruebaController',
	]);
});