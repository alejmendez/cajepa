<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
//use Modules\Beneficiario\Http\Requests\EstadoCuentaIngresosRequest;

//Modelos
//use Modules\Beneficiario\Model\estado_cuenta_ingresos;


class EstadoCuentaController extends Controller {
	protected $titulo = 'Estado Cuenta';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];
	public $js = ['estado_cuenta'];
	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {

		//$cedula = Auth::user()->cedula;


		$empleado = $this->db('nphiscon')
		->select('nphiscon.codemp','nphiscon.nomemp','nphiscon.codnom','nphiscon.fecnom')

		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		//->join('npdefcpt','npdefcpt.CODCON','=','npdefcpt.nomcon')
		//->where('nphiscon.CODEMP','LIKE','21009532%')
		->where('nphiscon.CODEMP','LIKE','17338695%')
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		->groupBy('nphiscon.codemp','nphiscon.nomemp','nphiscon.codnom','nphiscon.fecnom')
		->distinct()
		->first();

		 $ingreso = $this->db('nphiscon')
		 ->select('nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon')

		// ->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		/->join('npdefcpt','npdefcpt.CODCON','=','npdefcpt.nomcon')
		->where('nphiscon.CODEMP','LIKE','17338695%')
		->where('npdefcpt.nomcon','LIKE','')
		->where('npdefcpt.nomcon','LIKE','CAJA%DE%AHORRO%CAEJPA%')
		->whereYear('nphiscon.fecnom', '=', 2015)
		->groupBy('nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon')
		->distinct()
		->orderBy('nphiscon.fecnom','ASC')

		 ->get();

		$egreso = $this->db('nphiscon')
		->select('nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon')

		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		//->join('npdefcpt','npdefcpt.CODCON','=','npdefcpt.nomcon')
		->where('nphiscon.CODEMP','LIKE','17338695%')

		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		->where('npdefcpt.nomcon','LIKE','CUOTA%')
		//->whereYear('nphiscon.fecnom', '=', 2015)
		->groupBy('nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon')
		->orderBy('nphiscon.fecnom','ASC')
		->distinct()
		->get();

		$beneficiario = $this->db('nphiscon')
		->select('nphojint.codemp','nphojint.nomemp','npdefcpt.nomcon')
		->join('nphojint','nphiscon.codemp','=','nphojint.codemp')
		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		//->where('nphiscon.CODEMP','LIKE','19869498%')
		//->where('nphiscon.codcon','LIKE','%55')
		->whereYear('nphiscon.fecnom', '=', 2016)
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		//->groupBy('nphojint.codemp','nphojint.nomemp','npdefcpt.nomcon')
		->orderBy('nphojint.nomemp','ASC')
		->distinct('nphojint.codemp')
		->get();

		$prestamo = $this->db('nptippre')
		//->select('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->join('nphispre','nptippre.codtippre','=','nphispre.codtippre')
		->join('npdefcpt','nptippre.codcon','=','npdefcpt.codcon')
		//->where('NPHISCON.codemp','LIKE','19869498%')
		->whereYear('nphispre.fechispre', '=', 2016)
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		->where('nphispre.CODEMP','LIKE','17338695%')
		//->groupBy('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->orderBy('nphispre.fechispre','DESC')
		//->distinct()
		->get();

		$n = $this->db('nptippre')
		//->select('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->join('nphispre','nptippre.codtippre','=','nphispre.codtippre')
		->join('npdefcpt','nptippre.codcon','=','npdefcpt.codcon')
		//->where('NPHISCON.codemp','LIKE','19869498%')
		//->whereYear('nphispre.fechispre', '=', 2016)
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		->where('nphispre.CODEMP','LIKE','17338695%')
		//->groupBy('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->orderBy('nphispre.fechispre','DESC')
		//->distinct()
		->first();

		return $this->view('beneficiario::estado_cuenta', ['empleado'=>$empleado,'egreso'=>$egreso,'beneficiario'=>$beneficiario,'prestamo'=>$prestamo,'n'=>$n]);
	}
	
	protected function db($tabla = ''){
		$db = DB::connection('sima');
		
		if ($tabla !== ''){
			$db = $db->table($tabla);
		}

		return $db;
	}

	public function getBuscar(Request $request, $id = 0){
		$beneficiario = beneficiario::find($id);

		$nomina = DB::table('codigo_nomina')->select('codigo','id')->where('id', $beneficiario->nomina_id)->get();

		foreach ($nomina as $n) {
			
		$condicional= DB::table('codigo_condicional')->select('codigo','id', 'codigo_nomina_id')->where('codigo_nomina_id', $n->id)->get();
		}

		
		if ($beneficiario){
			return array_merge($beneficiario->toArray(), [
				'condicional'=>$condicional,
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(estado_cuenta_ingresosRequest $request){
		DB::beginTransaction();
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(estado_cuenta_ingresosRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$s = $this->db('nptippre')
		->select(['nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon'])
		->join('estado', 'estado.id', '=', 'municipio.estado_id')
		->where('nphiscon.CODEMP','LIKE','17338695%')
		->where('npdefcpt.nomcon','LIKE','CAJA%DE%AHORRO%CAEJPA%');

	    return Datatables::of($s)->make(true);
	}


	public function getCondicional(Request $request)
	{
		$sql = beneficiario::where('nomina_id', $request->id)
					->lists('codigo','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'condicional_id'=> $sql];
		}				
		
		return $salida;
	}
}