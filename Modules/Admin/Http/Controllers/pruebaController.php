<?php 
namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use SEO;
use Illuminate\Http\Request;
use Modules\Admin\Model\data_beneficiario;

class pruebaController extends Controller {
	protected $db;
	
	
	public function __construct(){
		parent::__construct();
		//$this->db = DB::connection('sima'); 17338695
	}


	public function getIndex(){

		
		$n = $this->db('nptippre')
		->select('nphispre.monpre','nphispre.saldo','nphispre.fechispre','nphispre.CODEMP')
		->join('nphispre','nphispre.codtippre','=','nptippre.codtippre')
		->join('npdefcpt','npdefcpt.codcon','=','nptippre.codcon')
		->where('nphispre.CODEMP','LIKE', '17045185%')
		->where('npdefcpt.codcon','%CAEJPA%' )
		->orderBy('nphispre.fechispre','DESC')
		->first();

		dd($n);
		
		$retpres = DB::table('data_beneficiario')
			->select('monto','fecha','concepto')
			->where('cedula', '=', '2184739')
			->get();

		$n = $this->db('nptippre')
		//->select('nphispre.monpre','nphispre.saldo','nphispre.fechispre')
		->join('nphispre','nptippre.codtippre','=','nphispre.codtippre')
		->join('npdefcpt','nptippre.codcon','=','npdefcpt.codcon')
		->where('nphispre.CODEMP', '=', '17338695')
		//->whereIn('npdefcpt.codcon','=', 'CUOTA PRESTAMO CAJA DE AHORRO CAEJPA-GEB. ')
		//->orderBy('nphispre.fechispre','DESC')
		->get();

		$empleado = $this->db('nphiscon')
		->select('nphiscon.codemp','nphiscon.nomemp','nphiscon.codnom','npdefcpt.nomcon','nphiscon.monto','nphiscon.saldo','nphiscon.fecnom','nphiscon.codcon')

		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		//->join('npdefcpt','npdefcpt.CODCON','=','npdefcpt.nomcon')
		->where('nphiscon.nomemp','LIKE','%astrid%')
		//->whereYear('nphiscon.fecnom', '=', 2015)
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		->groupBy('nphiscon.codemp','nphiscon.nomemp','nphiscon.codnom','npdefcpt.nomcon','nphiscon.monto','nphiscon.saldo','nphiscon.fecnom','nphiscon.codcon')
		->distinct()
		->first();

		$prueba1 = DB::table('data_beneficiario')
			->select('monto','fecha','concepto')
			//->where('cedula','=','761321')
			->where('concepto', 'LIKE','RETIRO%')
			->orderBy('fecha','DESC')
			//->first()
			->get();

 		$condicional = $this->db('npdefcpt')
 		->where('npdefcpt.nomcon','LIKE','%CAJA%')
 		->get();


 		$ingreso = $this->db('nphojint')
		//->select('nphiscon.fecnom','nphiscon.monto','nphiscon.codnom')
		//->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		//->join('npdefcpt','npdefcpt.CODCON','=','npdefcpt.nomcon')
		//->whereYear('nphiscon.fecnom', '=', 2015)
		->where('nphojint.nomemp','LIKE','%andreina%')
		//->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		//->where('nphiscon.nomcon','LIKE','%sueldo%')
		//->groupBy('nphiscon.fecnom','nphiscon.monto','nphiscon.codnom')
		->get();

		$beneficiario = $this->db('nphiscon')
		->select('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->join('nphojint','nphiscon.codemp','=','nphojint.codemp')
		//->join('NPNOMINA','NPHISCON.CODNOM','=','NPNOMINA.CODNOM')
		//->where('nphiscon.codemp','LIKE','19869498%')
		//->where('nphiscon.nomcon','LIKE','%SUELDO%')
		//->where('nphiscon.codcon','LIKE','%55')
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		//->groupBy('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		//->orderBy('nphiscon.fecnom','DESC')
		//->distinct()
		->first();


		$prestamo = $this->db('nptippre')
		->select('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		->join('nphispre','nptippre.codtippre','=','nphispre.codtippre')
		->join('npdefcpt','nptippre.codcon','=','npdefcpt.codcon')
		//->where('NPHISCON.codemp','LIKE','19869498%')
		->whereYear('nphispre.fechispre', '=', 2016)
		->where('npdefcpt.nomcon','LIKE','%CAEJPA%')
		//->where('nphispre.CODEMP','LIKE','17338695% 21578041')

		//->groupBy('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','npdefcpt.nomcon')
		//->distinct()
		->get();

		

	}

	protected function db($tabla = ''){
		$db = DB::connection('sima');
		
		if ($tabla !== ''){
			$db = $db->table($tabla);
		}

		return $db;
	}
	

}