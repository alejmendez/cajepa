<?php

namespace Modules\Admin\Http\Controllers;

//Dependencias
use Illuminate\Support\Facades\Auth;
use Session;

//Request
use Modules\Admin\Http\Requests\LoginRequest;

//Controlador Padre
use Modules\Admin\Http\Controllers\Controller;

//Modelos
use Modules\Admin\Model\app_historico;

class loginController extends Controller {
	public $autenticar = false;

	protected $redirectTo = '/';   //antes era /escritorio
	protected $redirectPath = '/';   //antes era /escritorio
	protected $prefijo = '';

	public function __construct() {
		//$this->middleware('guest', ['except' => 'getSalir']);
		$this->prefijo = \Config::get('admin.prefix');

		$this->redirectTo = $this->prefijo . $this->redirectTo;
		$this->redirectPath = $this->prefijo . $this->redirectPath;

		if (Auth::check()) {
			return redirect($this->prefijo . '/');   //antes era /escritorio
		}
	}

	public function getIndex() {
		if (Auth::check()) {
			return redirect($this->prefijo . '/');   //antes era /escritorio
		}

		return $this->view('admin::login');
	}

	public function getBloquear() {
		return $this->view('admin::bloquear');
	}

	public function getSalir() {
		Auth::logout();
		return redirect($this->prefijo . '/login');
	}

	public function postValidar(LoginRequest $request) {
		$credenciales = $request->credenciales();
		$autenticado = Auth::attempt($credenciales, $request->recordar());

		$idregistro = $autenticado ? '' : 'Clave:' . $credenciales['password'];
		$login = $credenciales['usuario'];

		App_historico::create([
			'tabla' => 'autenticacion',
			'concepto' => 'autenticacion',
			'idregistro' => $idregistro,
			'usuario' => $login,
		]);

		if ($autenticado) {
			Session::put('id_usuario', Auth::user()->id);
			Session::put('nombre_usuario', Auth::user()->nombre);

			return ['s' => 's'];
		}

		return ['s' => 'n', 'msj' => 'La combinacion de Usuario y Clave no Concuerdan.'];
	}
}