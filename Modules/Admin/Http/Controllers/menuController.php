<?php

namespace Modules\Admin\Http\Controllers;

//Controlador Padre
use Modules\Admin\Http\Controllers\Controller;

//Request
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\menuRequest;

//Modelos
use Modules\Admin\Model\app_estructura as estructura;
use Modules\Admin\Model\app_menu as menu;

class menuController extends Controller {
	protected $titulo = 'Menu';

	public $librerias = [
		'alphanum', 
		'datatables',
		'jstree'
	];

	public function getIndex() {
		return $this->view('admin::menu');
	}

	public function postBuscar(Request $request, $id = 0) {
		$rs = menu::find($id);

		if ($rs) {
			return array_merge($rs->toArray(), [
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}

	public function postCrear(menuRequest $request) {
		try {
			$data = $request->input('data');
			$ids = [];

			foreach ($data as $valor) {
				$ids[$valor['id']] = 0;

				if (!is_numeric($valor['padre'])) {
					$valor['padre'] = $ids[$valor['padre']];
				}

				if (!is_numeric($valor['id'])) {
					$menu = menu::create($valor);
				} else {
					$menu = menu::firstOrNew([
						'id' => $valor['id'],
					]);

					$menu->fill($valor);
					$menu->save();
				}

				$ids[$valor['id']] = $menu->id;
			}

			$eliminados = $request->input('eliminados');
			menu::destroy($eliminados);
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function postEliminar(Request $request, $id = 0) {
		try {
			$rs = menu::destroy($id);
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getArbol() {
		return menu::estructura();
	}
}