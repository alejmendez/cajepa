<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Controllers\Controller;

class escritorioController extends Controller {
	public $autenticar = false;

	protected $titulo = 'Escritorio';

	public function __construct() {
		parent::__construct();

		$this->middleware('auth');
	}

	public function getIndex() {
		return $this->view('admin::escritorio');
	}
}