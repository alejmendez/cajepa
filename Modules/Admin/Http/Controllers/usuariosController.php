<?php

namespace Modules\Admin\Http\Controllers;

//Dependencias
use DB;
use URL;
use Yajra\Datatables\Datatables;

//Controlador Padre
use Modules\Admin\Http\Controllers\Controller;

//Request
use Modules\Admin\Http\Requests\usuariosRequest;

//Modelos
use Modules\Admin\Model\app_usuario as usuario;
use Modules\Admin\Model\app_perfil as perfil;
use Modules\Admin\Model\app_menu as menu;
use Modules\Admin\Model\app_usuario_permisos as usuario_permisos;
use Modules\Admin\Model\app_usuario_perfil as usuario_info;

class usuariosController extends Controller {
	protected $titulo = 'Usuarios';

	public $librerias = [
		'alphanum', 
		'maskedinput', 
		'datatables', 
		'jstree',
	];
	
	public $css = [
		'perfil_user'
	];

	public function getIndex() {
		return $this->view('admin::usuarios');
	}

	public function getBuscar(usuariosRequest $request, $id = 1) {
		$rs = usuario::find($id);

		$informacion = $rs->informacion;

		if ($rs){
			$usuario = $rs->toArray();
			$usuario['foto'] = URL::to("public/img/usuarios/" . $usuario['foto']);

			$permisos = [];
			$_permisos = $rs->permisos->toArray();
			
			if ($informacion) {
				$informacion = $informacion->toArray();
				unset($informacion['id'], $informacion['app_usuario_id']);
			}else{
				$informacion = [];
			}

			foreach ($_permisos as $_permiso) {
				$permisos[] = $_permiso['ruta'];
			}

			return array_merge($usuario, $informacion, [
				'permisos' => $permisos,
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}
	

	public function postCrear(usuariosRequest $request) {
		DB::beginTransaction();
		try {
			$name="user.png";
			
			if($request->file('foto'))
			{ 
				$file = $request->file('foto');
	            $name =  $request->usuario.'.'.$file->getClientOriginalExtension();
	            $path= public_path() . '/img/usuarios/';
	            $file->move($path, $name);
	 			$filename=$path.$name;
	 			chmod($filename, 0777);
 			}

 			$usuario = $request->all();
 			$usuario['foto'] = $name;
 			

			$rs = usuario::create($usuario);
			$id = $rs['id'];

			//$informacion = $request->all();
 			
			usuario_info::create([
				'app_usuario_id'	=>$id,
				'apellido'			=>$request->apellido, 
				'sexo'				=>$request->sexo, 
				'edo_civil'			=>$request->estado,  
				'direccion'			=>$request->direccion, 
				'facebook'			=>$request->facebook, 
				'instagram'			=>$request->instagram, 
				'twitter'			=>$request->twitter
				]);

			$this->procesar_permisos($request, $id);
			
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
	

	public function putActualizar(usuariosRequest $request, $id = 0) {
		DB::beginTransaction();
		try {
			
			$usuario = $request->all();
			
			if($usuario['password'] ==""){
				unset($usuario['password']);
			}
				
			if($request->file('foto'))
			{ 
				$file = $request->file('foto');
	            $name =  $request->usuario.'.'.$file->getClientOriginalExtension();
	            $path= public_path() . '/img/usuarios/';
	            $file->move($path, $name);
	 			$filename=$path.$name;
	 			//chmod($filename, 0777);
	 			
 				$usuario['foto'] = $name;

 			}else{
 				unset($usuario['foto']);
 			}

			unset($usuario['usuario']);
			

			usuario::find($request->id)->update($usuario);
			
			usuario_info::firstOrNew(['app_usuario_id' => $request->id])
			->fill([ 
				'apellido'			=>$request->apellido,
				'sexo'				=>$request->sexo,
				'edo_civil'			=>$request->estado,
				'direccion'			=>$request->direccion,
				'facebook'			=>$request->facebook,
				'instagram'			=>$request->instagram,
				'twitter'			=>$request->twitter
			])
			->save();

			$this->procesar_permisos($request, $id);
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	

	protected function procesar_permisos($request, $id) {
		$permisos = explode(',', $request->input('permisos'));

		$permiso_perfil = usuario_permisos::where('app_usuario_id', $id)->delete();

		foreach ($permisos as $permiso) {
			$permiso = trim($permiso);

			usuario_permisos::create([
				'app_usuario_id' => $id,
				'ruta' => trim($permiso),
			]);
		}
	}

	public function deleteEliminar(usuariosRequest $request, $id = 0) {
		try {
			$rs = usuario::destroy(intval($id));
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function perfiles() {
		return perfil::lists('nombre', 'id');
	}

	public function getArbol() {
		return menu::estructura(true);
	}

	public function getDatatable() {
		$sql = usuario::select('id', 'cedula', 'nombre', 'usuario');

		return Datatables::of($sql)->setRowId('id')->make(true);
	}
}