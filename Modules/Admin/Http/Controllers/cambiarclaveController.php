<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Model\app_usuario as usuario;
use Modules\Admin\Http\Controllers\Controller;
use Modules\Backend\Http\Requests\cambiar_claveRequest;

class cambiarclaveController extends Controller {
	public $autenticar = false;

	protected $titulo = 'Cambiar Clave';

	public function getIndex() {
		return $this->view('admin::cambiar_clave');
	}

	public function postGuardar(cambiar_claveRequest $request, $id = 0) {
		try {
			$usuario = usuario::find(\Auth::user()->id);
			$usuario->password = $request->input('password');
			$usuario->save();
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}
}