<?php

namespace Modules\Admin\Http\Controllers;

//Dependencias
use DB;
use Yajra\Datatables\Datatables;

//Controlador Padre
use Modules\Admin\Http\Controllers\Controller;

//Request
use Modules\Admin\Http\Requests\perfilesRequest;
use Illuminate\Http\Request;

//modelos
use Modules\Admin\Model\app_perfil as perfil;
use Modules\Admin\Model\app_perfiles_permisos as perfil_permisos;
use Modules\Admin\Model\app_menu as menu;

class perfilesController extends Controller {
	protected $titulo = 'Perfiles';

	public $librerias = [
		'datatables',
		'jstree'
	];

	public function getIndex() {
		return $this->view('admin::perfiles');
	}

	public function getBuscar(Request $request) {
		$perfil = perfil::find($request->id);

		if ($perfil) {
			$permisos = [];
			$_permisos = $perfil->permisos->toArray();

			foreach ($_permisos as $_permiso) {
				$permisos[] = $_permiso['ruta'];
			}

			return array_merge($perfil->toArray(), [
				'permisos' => $permisos,
				's' => 's',
				'msj' => trans('controller.buscar'),
			]);
		}

		return trans('controller.nobuscar');
	}

	public function postCrear(perfilesRequest $request) {
		DB::beginTransaction();

		try {
			$perfil = perfil::create($request->all());
			$id = $perfil['id'];

			$this->procesar_permisos($request, $id);
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(perfilesRequest $request, $id = 0) {
		DB::beginTransaction();

		try {
			$perfil = perfil::find($id)->update($request->all());

			$this->procesar_permisos($request, $id);
		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();
		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	protected function procesar_permisos($request, $id) {
		$permisos = explode(',', $request->input('permisos'));

		$permiso_perfil = perfil_permisos::where('app_perfil_id', $id)->delete();

		foreach ($permisos as $permiso) {
			$permiso = trim($permiso);

			perfil_permisos::create([
				'app_perfil_id' => $id,
				'ruta' => trim($permiso),
			]);
		}
	}

	public function deleteEliminar(Request $request, $id) {
		try {
			perfil::destroy($id);
		} catch (Exception $e) {
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getArbol() {
		return menu::estructura(true);
	}

	public function getDatatable() {
		$sql = perfil::select('id', 'nombre');

		return Datatables::of($sql)->setRowId('id')->make(true);
	}
}