<?php

namespace Modules\Backend\Http\Requests;
 
use App\Http\Requests\Request;
 
class cambiar_claveRequest extends Request {
	protected $rules = [
		'password' => ['required', 'confirmed', 'regex:/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/']
	];

	public function rules(){
		return $this->reglas();
	}
}