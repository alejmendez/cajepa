<?php namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;

class crearinfoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			
			'cedula'   	=> ['required', 'integer' ],
			'nombre'   	=> ['required', 'nombre', 'min:3', 'max:50'],
			'apellido' 	=> ['min:3', 'max:50'],
			'sexo'     	=> ['integer'],
			'estado'    => ['integer'],
			'correo'   	=> ['email', 'max:50'],
			'telefono' 	=> ['telefono'],
			'direccion' => ['max:250'],
			'fb'	    => ['max:250'],
			'instagram'	=> ['max:250'],
			'tw'		=> ['max:250'],
		];
	}

}
