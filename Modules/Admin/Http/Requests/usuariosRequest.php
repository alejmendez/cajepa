<?php

namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;

class usuariosRequest extends Request {
	protected $rules = [
		'usuario' => ['required', 'min:3', 'max:15', 'regex:/^[a-z\d_]{4,15}$/i', 'unique:app_usuario,usuario'],
		'nombre' => ['required', 'nombre', 'min:3', 'max:50'],
		'cedula' => ['required', 'integer', 'unique:app_usuario,cedula'],
		'password' => ['required', 'password', 'min:8', 'max:50'],
		'telefono' => ['telefono'],
		'correo' => ['email', 'max:50', 'unique:app_usuario,correo'],
		'app_perfil_id' => 'required',
	];

	public function rules() {
		$rules = parent::rules();

		$usuario = \Auth::user();

		if ($usuario->super === 's'){
			$rules['password'] = ['max:50'];
		}

		switch ($this->method()){
			case 'PUT':
			case 'PATCH':
				if ($this->input('password') == '') {
					unset($rules['password']);
				}

				break;
		}

		return $rules;
	}
}