<?php

namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;

class perfilesRequest extends Request {
	protected $rules = [
		'nombre' => ['required', 'nombre', 'min:3', 'max:50'],
	];

	public function rules() {
		return $this->rules;
	}
}