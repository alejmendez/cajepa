@extends('admin::layouts.default')
@section('content')
	<div class="row">
		<div class="col-sm-12">
			<h1 class="page-header">Cambio de Contrase&ntilde;a de Usuario</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-edit"></i> Cambio de Contrase&ntilde;a de Usuario
				</li>
			</ol>

			<p class="bg-info">
				La <b>Contrase&ntilde;a</b> debe contener al menos un letra en Mayuscula, una letra en Minuscula, al menos un numero y su longitud debe ser mayor a 8 caracteres.
			</p>
		</div>

		<form id="formulario" name="formulario" method="POST">
			<div class="form-group col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
				<label for="password">Contrase&ntilde;a <i class="fa fa-asterisk requerido"></i></label>
				<input id="password" name="password" class="form-control" type="password" value="">
			</div>

			<div class="form-group col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
				<label for="password_confirmation">Confirmar Contrase&ntilde;a <i class="fa fa-asterisk requerido"></i></label>
				<input id="password_confirmation" name="password_confirmation" class="form-control" type="password" value="">
			</div>
		</form>

		<div id="botonera" class="form-group col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12" style="text-align: center">
			<button id="guardar" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Guardar</button>
		</div>
	</div>
@endsection

@push('css')
<style type="text/css">
.bg-info{
	padding: 8px 15px;
	margin-bottom: 20px;
	text-align: justify;
	border-radius: 4px;
}
</style>
@endsection

@push('js')
<script>
var aplicacion;
$(function() {
	aplicacion = new app("formulario");
});
</script>
@endpush