@extends('admin::layouts.default')
@section('content')
<?php 
dd($s);
 ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
  <!-- Nav tabs -->
          <h4 class="text-primary"><strong>Panel de Control</strong></h4>
          <ul class="nav nav-pills nav-stacked" role="tablist">
            <li role="presentation" class="active"><a href="#homeDatos" aria-controls="homeDatos" role="tab" data-toggle="tab">Datos Personales</a></li>
            <li role="presentation"><a href="#saldoDatos" aria-controls="saldoDatos" role="tab" data-toggle="tab">Consulta de Saldo</a></li>

            <li role="presentation"><a href="#tablaDatos" aria-controls="tablaDatos" role="tab" data-toggle="tab">Estado Cuenta</a></li>
            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Cambiar de clave</a></li>
          </ul>
     </div>
  <div class="col-md-8">

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="homeDatos">
        <table class="table table-hover">
        <h4 class="text-primary"><strong>Datos del Beneficiario</strong></h4>
        <tr>
            <td class="active">Nombre y Apellido</td>   
            <td class="success"><strong>{{$empleado->nomemp}}</strong></td>
            </tr>
            <!--<tr>
                <td class="active">Apellido</td>
                <td class="success">Baez Bernardo</td>
            </tr>-->
            <tr>
            <td class="active">Cedula</td>  
            <td class="success">{{$empleado->codemp}}</td>
            </tr>
            <tr>
            <td class="active">Nro de Nomina</td>   
            <td class="success">{{$empleado->codnom}}</td>
            </tr>
            <tr>
                <td class="active">Fecha de Ingreso</td>
                <td class="success">{{ \Carbon\Carbon::parse($empleado->fecnom)->format('d/m/Y')}}</td>
            </tr>
</table>

    </div>
    <div role="tabpanel" class="tab-pane" id="tablaDatos">
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
    <a href="#ingreso" aria-controls="ingreso" role="tab" data-toggle="tab"><strong>Ingreso Patron/Qnal. </strong></a>
    </li>
    <li role="presentation">
    <a href="#egreso" aria-controls="egreso" role="tab" data-toggle="tab"><strong>Pago de prestamos</strong></a>
    </li>
    <li role="presentation">
    <a href="#parcial" aria-controls="parcial" role="tab" data-toggle="tab"><strong>Retiro parcial/prestamos</strong></a>
    </li>
     <li role="presentation">
    <a href="#bene" aria-controls="bene" role="tab" data-toggle="tab"><strong>Tabla Beneficiario CAEJPA</strong></a>
    </li>


  </ul>


  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="ingreso">
        <table class="table table-hover">
            <thead>
            <td>Fecha de transacción</td>
            <td>Monto</td>
            <td>Condicional</td>
            <td>Condicional</td>
            </thead>    
            <?php 
            $resultado = 0;

             ?>
        @foreach($ingreso as $ing)
        <?php 
        $resultado += $ing->monto; 
         ?>
            <tr>
                <td class="success">{{ \Carbon\Carbon::parse($ing->fecnom)->format('d/m/Y')}}</td>       
                <td class="success suma">{{$ing->monto}}bs</td> 
                <td class="success">{{$ing->codcon}}</td> 
                <td class="success">{{$ing->nomcon}}</td> 
            </tr>
        @endforeach
       <!--  <?php 
        $resultado1 = $resultado / 2;
        echo $resultado1;
        echo "______";
        echo $resultado;
         ?> -->
    </table>
    </div>
    <div role="tabpanel" class="tab-pane" id="egreso">
        <table class="table table-hover">
            <thead>
            <td>Fecha de transacción</td>
            <td>Monto</td>
    		<td>Condicional</td>
            <td>Condicional</td>
    		</thead>
    	   @foreach($egreso as $egre)
            <tr>
                <td class="success">{{ \Carbon\Carbon::parse($egre->fecnom )->format('d/m/Y')}}</td>       
                <td class="success">{{$egre->monto}}bs</td> 
                <td class="success">{{$egre->codcon}}</td>
                <td class="success">{{$egre->nomcon}}</td> 
            </tr>
        @endforeach
  
</table>
    </div>
      <div role="tabpanel" class="tab-pane" id="bene">
        <table class="table table-hover">
            <thead>
            <td>Nombe</td>
            <td>Cedula</td>

            </thead>
           @foreach($beneficiario as $bene)
            <tr>
                <td class="success">{{$bene->codemp}}</td> 
                <td class="success">{{$bene->nomemp}}</td>
            </tr>
          @endforeach
</table>
    </div>
        <div role="tabpanel" class="tab-pane" id="parcial">
        <div class="tabla"></div>

    </div>
  </div>

</div>
</div>
    <div role="tabpanel" class="tab-pane" id="saldoDatos">
    <h4 class="text-primary"><strong>Aportes y 80%</strong></h4>
    <table class="table table-hover table-responsive">
    <tr>
    		<td class="active">Ahorro Socio</td>	
    		<td class="success"><?php echo $resultado1; ?></td>
    		</tr>
    		<tr>
    			<td class="active">Aporte Patronal</td>
    			<td class="success"><?php echo $resultado1; ?></td>
    		</tr>
    		<tr>
    		<td class="active">Ahorro Voluntario</td>	
    		<td class="success">0.0</td>
    		</tr>
    		<tr>
            <?php 
                $resultado2 = $resultado * 0.20;
                $resultado3 = $resultado - $resultado2; 
                ?>
    		<td class="active">TOTAL AHORROS ACUMULADOS</td>	
    		<td class="success"><?php echo $resultado; ?></td>
    		</tr>
    		<tr>
    			<td class="active">80% AHORROS DISPONIBLES</td>
    			<td class="success"><strong><?php echo $resultado3; ?></strong></td>
    		</tr>
    		</table>
    		 <h4 class="text-primary"><strong>Prestamos</strong></h4>
            <table class="table table-bordered table-responsive">
    		<tr>
    		<td class="danger">Tipo de Prestamos</td>
    		<td class="danger">Fecha</td>
    		<td class="danger">Monto</td>	
    		<td class="danger">Cuotas</td>	
    		<td class="danger">Saldo</td>	
    		</tr>
  
            <tr>
    			<td class="">CUOTA ESPECIALES PREST. FIANZA</td>
    			<td class="">{{\Carbon\Carbon::parse($n->fechispre)->format('d/m/Y') }}</td>
    			<td class="">200.000</td>
    			<td class="">{{$n->monpre}}</td>
    			<td class="success"><strong>{{$n->saldo}}</strong></td>
    		</tr>
            <tr>
                
                <td class="">Total de Prestamos</td>
                <td class=""></td>
                <td class=""></td>
                <td class=""></td>
                <td class="success">50.000</td>
            </tr>
    	
    		</table>
    		<h4 class="text-primary"><strong>Disponibilidad</strong></h4>
            <table class="table table-hover table-responsive">
            <tr>
    		<td class="active">Fecha del ultimo Retiro</td>	
    		<td class="success">01/01/01</td>
    		</tr>
    		<tr>
    			<td class="active">Total de Prestamos</td>
    			<td class="success">50.000</td>
    		</tr>
    		<tr>
    		<td class="active">Finanzas</td>	
    		<td class="success"></td>
    		</tr>
    		<tr>
    		<td class="active">Disponibilida neta</td>	
    		<td class="success">45.000</td>
    		</tr>
    		</table>
    </div>
    <div role="tabpanel" class="tab-pane" id="settings">


    </div>
  </div>


		</div>
       
        </div></div>
		
</div>
@endsection