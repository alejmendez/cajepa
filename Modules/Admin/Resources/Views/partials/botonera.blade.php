<div id="botonera">
	<div class="btn-group btn-group-solid">
		<button id="limpiar" class="btn default tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.clean.title') }}">
			<i class="fa fa-file-o"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.clean.btn') }}</span>
		</button>

		<button id="guardar" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.save.title') }}">
			<i class="fa fa-floppy-o"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.save.btn') }}</span>
		</button>

		<button id="eliminar" class="btn red tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.remove.title') }}">
			<i class="fa fa-trash"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.remove.btn') }}</span>
		</button>

		<button id="buscar" class="btn green tooltips" data-container="body" data-placement="top" data-original-title="{{ Lang::get('backend.btn_group.search.title') }}">
			<i class="fa fa-search"></i>
			<span class="visible-lg-inline visible-md-inline">{{ Lang::get('backend.btn_group.search.btn') }}</span>
		</button>
		@stack('botones')
	</div>
</div>