<?php namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_menu as menu;

class menuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = menu::create([
			'nombre' => 'Administrador',
			'padre' => 0,
			'posicion' => 0,
			'direccion' => '#Administrador',
			'icono' => 'fa fa-gear'
		]);

			menu::create([
				'nombre' => 'Menu',
				'padre' => $menu->id,
				'posicion' => 0,
				'direccion' => 'menu',
				'icono' => 'fa fa-sitemap'
			]);

			menu::create([
				'nombre' => 'Perfiles',
				'padre' => $menu->id,
				'posicion' => 1,
				'direccion' => 'perfiles',
				'icono' => 'fa fa-users'
			]);

			menu::create([
				'nombre' => 'Usuarios',
				'padre' => $menu->id,
				'posicion' => 2,
				'direccion' => 'usuarios',
				'icono' => 'fa fa-user'
			]);
    }
}
