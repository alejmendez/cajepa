<?php namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Model\app_usuario as usuario;
use Modules\Admin\Model\app_usuario_perfil as usuario_info;

class usuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = usuario::create([
			'usuario' => 'admin',
			'nombre' => 'Administrador',
			'password' => 'admin',
			'cedula' => 12345678,
			'correo' => 'admin@gmail.com',
			'telefono' => '0414-123-1234',
			'autenticacion' => 'B',
			'app_perfil_id' => 1,
			'super' => 's'
		]);

		 usuario_info::create([
        	'app_usuario_id'=> $usuario->id,
			'apellido'=> '',
			'sexo'=> '',
			'edo_civil'=> '',
			'direccion'=> '',
			'facebook'=> '',
			'instagram'=> '',
			'twitter'=> ''
        ]);
    }
}
