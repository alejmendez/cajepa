<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->integer('user_id')->nullable();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->text('payload');
            $table->integer('last_activity');
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        Schema::create('app_menu', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('nombre', 50);
            $table->integer('padre')->unsigned(); //->nullable();
            $table->integer('posicion')->unsigned(); //->nullable();
            $table->string('direccion', 50)->unique();
            $table->string('icono', 50);
            
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('app_perfil', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('nombre', 50)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('app_usuario', function(Blueprint $table){
            $table->increments('id');
            
            $table->string('usuario', 50)->unique();
            $table->string('nombre', 50);
            $table->string('password', 60);
            $table->integer('cedula')->unsigned()->unique();
            $table->string('correo', 50)->nullable()->unique();
            $table->string('telefono', 15)->nullable();
            $table->string('foto')->default('user.png');
            $table->integer('app_perfil_id')->unsigned()->nullable();

            $table->char('autenticacion', 1)->default('l');

            $table->char('super', 1)->default('n');
            
            $table->rememberToken();
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('app_perfil_id')
                ->references('id')->on('app_perfil')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('app_usuario_info', function(Blueprint $table){
            $table->increments('id');
            
            $table->integer('app_usuario_id')->unsigned()->unique();
            $table->string('apellido', 100)->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('edo_civil',2)->nullable();  
            $table->string('direccion')->nullable();    
            $table->string('facebook')->nullable(); 
            $table->string('instagram')->nullable();    
            $table->string('twitter')->nullable();
            
        
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('app_usuario_id')
                ->references('id')->on('app_usuario')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        
        Schema::create('app_perfiles_permisos', function(Blueprint $table){
            $table->integer('app_perfil_id')->unsigned();
            $table->string('ruta', 200);

            $table->timestamps();
            //$table->softDeletes();
            
            $table->foreign('app_perfil_id')
                ->references('id')->on('app_perfil')
                ->onDelete('cascade')->onUpdate('cascade');
        });
        
        Schema::create('app_usuario_permisos', function(Blueprint $table){
            $table->integer('app_usuario_id')->unsigned();
            $table->string('ruta', 200);

            $table->timestamps();
            //$table->softDeletes();
            
            $table->foreign('app_usuario_id')
                ->references('id')->on('app_usuario')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('app_historico', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('tabla', 50);
            $table->string('concepto', 50);
            $table->string('idregistro', 50);
            $table->string('usuario', 50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('app_historico');
        
        Schema::dropIfExists('app_perfiles_permisos');
        Schema::dropIfExists('app_usuario_permisos');
        
        Schema::dropIfExists('app_estructura');
        Schema::dropIfExists('app_menu');
        Schema::dropIfExists('app_usuario');
        Schema::dropIfExists('app_usuario_info');
        Schema::dropIfExists('app_perfil');

        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('sessions');
    }
}
