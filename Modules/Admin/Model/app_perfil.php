<?php 

namespace Modules\Admin\Model;
use Modules\Admin\Model\modelo;

class app_perfil extends modelo{
	protected $table = 'app_perfil';
	protected $fillable = ['nombre'];

	public function permisos(){
		return $this->hasMany('Modules\Admin\Model\app_perfiles_permisos');
		
		// hasMany = "tiene muchas" | hace relacion desde el maestro hasta el detalle
		//return $this->hasMany('Modules\Admin\Model\App_usuario_permisos');
	}
}
