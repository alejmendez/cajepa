<?php 

namespace Modules\Admin\Model;

use Modules\Admin\Model\modelo;

class App_usuario_perfil extends modelo{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'app_usuario_info';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['app_usuario_id', 'apellido', 'sexo', 'edo_civil',  'direccion', 'facebook', 'instagram', 'twitter' ];


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];



	public function informacion(){
		//hasMany = "tiene muchas" | hace relacion desde el maestro hasta el detalle
		//belongsTo = pertenece A
		//hasOne = tiene un
		return $this->belongsTo('App\Model\App_usuario');
	}
}
   