<?php 

namespace Modules\Admin\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Modules\Admin\Model\modelo;

class app_usuario extends modelo implements AuthenticatableContract, CanResetPasswordContract {
	use Authenticatable, CanResetPassword;
	
	protected $table = 'app_usuario';
	protected $fillable = ['usuario', 'nombre', 'password', 'cedula', 'correo', 'telefono', 'foto' ,'autenticacion', 'super', 'app_perfil_id'];

	protected $hidden = ['password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'];

	public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }


	public function permisos(){
		// hasMany = "tiene muchas" | hace relacion desde el maestro hasta el detalle
		return $this->hasMany('Modules\Admin\Model\app_usuario_permisos');
	}

	public function informacion(){
		// hasMany = "tiene muchas" | hace relacion desde el maestro hasta el detalle
		return $this->hasOne('Modules\Admin\Model\app_usuario_perfil');
	}
}
