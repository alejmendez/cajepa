<?php

namespace Modules\Admin\Model;

use Carbon\Carbon; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Model\app_historico;

class modelo extends Model
{
	use SoftDeletes;
	protected $fillable = [];

	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
	protected $dates = ['deleted_at'];
	protected $historico = true;
	//protected $dateFormat = 'd-m-Y';
	
	public $timestamps = false;

	public static function boot(){
		parent::boot();

		//--- Modificando los campos de fecha y hora
		// Creando
		static::creating(function($model) {
			$model->created_at = Carbon::now()->format('Y-m-d H:i:s');
			$model->updated_at = Carbon::now()->format('Y-m-d H:i:s');
			return true;
		});

		// Actualizando
		static::updating(function($model) {
			$model->updated_at = Carbon::now()->format('Y-m-d H:i:s');
			return true;
		});

		//--- Creando un registro en el historico de la aplicacion
		// Creado
		static::created(function($model) {
			$model->historico('creado', $model->id);
			return true;
		});

		// Actualizado
		static::updated(function($model) {
			$model->historico('actualizado', $model->id);
			return true;
		});

		// Eliminado
		static::deleted(function($model) {
			$model->historico('eliminado', $model->id);
			return true;
		});
	}

	public function historico($concepto, $id){
		if ($this->historico){
			$usuario = \Auth::user();
			
			$login = is_null($usuario) ? 'Invitado' : $usuario->usuario;
			
			App_historico::create([
				'tabla' => $this->table,
				'concepto' => $concepto,
				'idregistro' => $id,
				'usuario' => $login,
			]);
		}
	}
}