var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		/*'botones' : true,
		'teclado' : true,
		'medotos' : {
			'buscar' : 'buscar',
			'guardar' : 'crear',
			'actualizar' : 'actualizar',
			'eliminar' : 'eliminar',
		},*/
		'antes' : function(){},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar' : function(){},
		'guardar' : function(){},
		'eliminar' : function(){}
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + 'datatable',
		columns: [{'data':'nombres','name':'nombres'},{'data':'apellidos','name':'apellidos'},{'data':'cedula','name':'cedula'}]
	});

});
$('#cedula').change(function(){
		aplicacion.selectCascada($(this).val(), 'condicional_id','condicional');
	})
