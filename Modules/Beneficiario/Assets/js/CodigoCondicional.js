var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		/*'botones' : true,
		'teclado' : true,
		'medotos' : {
			'buscar' : 'buscar',
			'guardar' : 'crear',
			'actualizar' : 'actualizar',
			'eliminar' : 'eliminar',
		},*/
		'antes' : function(){},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar' : function(){},
		'guardar' : function(){},
		'eliminar' : function(){}
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + 'datatable',
		columns: [
			{'data':'nombre','name':'codigo_condicional.nombre'},
			{'data':'nombre_nomina','name':'codigo_nomina.nombre'},
			{'data':'codigo','name':'codigo_nomina.codigo'}

			]
	});
});

/*'codigo_condicional.nombre','codigo_nomina.nombre as nombre_nomina' ,'codigo_nomina.codigo'*/