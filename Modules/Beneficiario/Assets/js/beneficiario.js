var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		/*'botones' : true,
		'teclado' : true,
		'medotos' : {
			'buscar' : 'buscar',
			'guardar' : 'crear',
			'actualizar' : 'actualizar',
			'eliminar' : 'eliminar',
		},*/
		'antes' : function(){},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar' : function(){},
		'guardar' : function(){},
		'eliminar' : function(){}
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + 'datatable',
		columns: [{'data':'cedula','name':'cedula'},{'data':'nombres','name':'nombres'},{'data':'apellidos','name':'apellidos'},{'data':'municipio_id','name':'municipio_id'},{'data':'nomina_id','name':'nomina_id'},{'data':'codigo_exp','name':'codigo_exp'},{'data':'fecha_ingreso','name':'fecha_ingreso'},{'data':'tipo_nomina_id','name':'tipo_nomina_id'},{'data':'estatus_id','name':'estatus_id'},{'data':'observacion','name':'observacion'}]
	});
	$('#estado_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'municipio_id','municipio');
	});
	//$('#nomina_id').change(function(){
	//	aplicacion.selectCascada($(this).val(), 'condicional_id','condicional');
	//});
});