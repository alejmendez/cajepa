<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class codigo_condicional extends Model
{
	protected $table = 'codigo_condicional';
    protected $fillable = ['id','nombre','codigo','codigo_nomina_id'];

    protected $hidden = [];
}