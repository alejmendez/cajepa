<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class codigo_nomina extends Model
{
	protected $table = 'codigo_nomina';
    protected $fillable = ['id','nombre','codigo'];

    protected $hidden = [];
}