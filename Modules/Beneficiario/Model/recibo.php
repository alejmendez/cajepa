<?php 
namespace Modules\Beneficiario\Model;
   
use Illuminate\Database\Eloquent\Model;

class recibo extends Model {

	protected $table = 'recibo';

    protected $fillable = ['cheque','banco_id','data_beneficiario_id','total_ahorros','prestamos', 'interes', 'retiro_parcial', 'prestamos_80', 'ahorro_restantes', 'fecha', 'año', 'nombre', 'cedula'];


}