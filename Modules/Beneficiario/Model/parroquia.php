<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class parroquia extends Model
{
	protected $table = 'parroquia';
    protected $fillable = ['id','nombre','municipio_id'];

    protected $hidden = ['created_at','updated_at'];
}