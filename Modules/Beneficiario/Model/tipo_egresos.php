<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class tipo_egresos extends Model
{
	protected $table = 'tipo_egresos';
    protected $fillable = ['id','nombre'];

    protected $hidden = [];
}