<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class banco extends Model
{
	protected $table = 'banco';
    protected $fillable = ['id','nombre','cuenta'];

    protected $hidden = [];
}