<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class nomina extends Model
{
	protected $table = 'tipo_nomina';
    protected $fillable = ['id','nombre'];

    protected $hidden = [];
}