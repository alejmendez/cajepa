<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class beneficiario extends Model
{
	protected $table = 'beneficiario';
    protected $fillable = ['id','cedula','nombres','apellidos','municipio_id','nomina_id','codigo_exp','fecha_ingreso','tipo_nomina_id','estatus_id','observacion'];

    protected $hidden = ['created_at','updated_at'];
}