<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class municipio extends Model
{
	protected $table = 'municipio';
    protected $fillable = ['id','nombre','estado_id'];

    protected $hidden = [];
}