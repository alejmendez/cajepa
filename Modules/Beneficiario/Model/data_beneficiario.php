<?php 
namespace Modules\Beneficiario\Model;
   
use Illuminate\Database\Eloquent\Model;

class data_beneficiario extends Model {

	protected $table = 'data_beneficiario';

    protected $fillable = ['cedula','nombre','concepto','fecha','monto'];

}