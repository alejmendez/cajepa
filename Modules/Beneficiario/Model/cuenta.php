<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class cuenta extends Model
{
	protected $table = 'cuenta';
    protected $fillable = ['id','banco_id','numero'];

    protected $hidden = [];
}