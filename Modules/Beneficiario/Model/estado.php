<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
	protected $table = 'estado';
    protected $fillable = ['id','nombre'];

    protected $hidden = [];
}