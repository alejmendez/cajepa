<?php
namespace Modules\Beneficiario\Model;

use Illuminate\Database\Eloquent\Model;

class estado_cuenta_ingresos extends Model
{
	protected $table = 'estado_cuenta_ingresos';
    protected $fillable = ['id','beneficiario_id','fecha_aporte','condicional_id','aporte','observacion'];

    protected $hidden = ['created_at','updated_at'];
}