<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\BeneficiarioRequest;

//Modelos
use Modules\Beneficiario\Model\beneficiario;
use Modules\Beneficiario\Model\estado;
use Modules\Beneficiario\Model\municipio;
use Modules\Beneficiario\Model\codigo_nomina;
use Modules\Beneficiario\Model\codigo_condicional;
use Modules\Beneficiario\Model\tipo_nomina;
use Modules\Beneficiario\Model\estatus;

class BeneficiarioController extends Controller {
	protected $titulo = 'Beneficiario';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];
	public $js = ['beneficiario'];
	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::beneficiario');
	}

	public function getBuscar(Request $request, $id = 0){
		$beneficiario = beneficiario::find($id);
		
		if ($beneficiario){
			return array_merge($beneficiario->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(beneficiarioRequest $request){
		DB::beginTransaction();
		try{
			$beneficiario = beneficiario::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(beneficiarioRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$beneficiario = beneficiario::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$beneficiario = beneficiario::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

 	public function estados(){

 		return estado::lists('nombre', 'id');


 	}
 	public function nominas(){

 		return codigo_nomina::lists('codigo', 'id');


 	}
 	public function tipo_nomina(){

 		return tipo_nomina::lists('nombre', 'id');


 	}
 	public function estatus(){

 		return estatus::lists('nombre', 'id');


 	}

 	public function getMunicipio(Request $request){
		$sql = municipio::where('estado_id', $request->id)
					->lists('nombre','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'municipio_id'=> $sql];
		}				
		
		return $salida;
	}
/*	public function getCondicional(Request $request){
		$sql = codigo_condicional::where('codigo_nomina_id', $request->id)
					->lists('nombre','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'condicional_id'=> $sql];
		}				
		
		return $salida;
	}
*/

	public function getDatatable(){
		$sql = beneficiario::select([
			'id','cedula','nombres','apellidos','municipio_id','nomina_id','codigo_exp','fecha_ingreso','tipo_nomina_id','estatus_id','observacion'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}