<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\EstadoCuentaIngresosRequest;

//Modelos
use Modules\Beneficiario\Model\estado_cuenta_ingresos;
use Modules\Beneficiario\Model\beneficiario;
use Modules\Beneficiario\Model\codigo_condicional;

class EstadoCuentaIngresosController extends Controller {
	protected $titulo = 'Estado Cuenta Ingresos';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];
	public $js = ['estado_cuenta_ingresos'];
	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::estado_cuenta_ingresos');
	}

	public function getBuscar(Request $request, $id = 0){
		$beneficiario = beneficiario::find($id);

		$nomina = DB::table('codigo_nomina')->select('codigo','id')->where('id', $beneficiario->nomina_id)->get();

		foreach ($nomina as $n) {
			
		$condicional= DB::table('codigo_condicional')->select('codigo','id', 'codigo_nomina_id')->where('codigo_nomina_id', $n->id)->get();
		}

		
		if ($beneficiario){
			return array_merge($beneficiario->toArray(), [
				'condicional'=>$condicional,
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(estado_cuenta_ingresosRequest $request){
		DB::beginTransaction();
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(estado_cuenta_ingresosRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = beneficiario::select([
			'id','nombres','apellidos','cedula'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}

	public function getCondicional(Request $request)
	{
		$sql = beneficiario::where('nomina_id', $request->id)
					->lists('codigo','id')
					->toArray();

		$salida = ['s' => 'n' , 'msj'=> 'El Municipio no contiene Parroquias'];
		
		if($sql){
			$salida = ['s' => 's' , 'msj'=> 'Parroquias encontradas', 'condicional_id'=> $sql];
		}				
		
		return $salida;
	}
}