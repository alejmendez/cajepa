<?php 

namespace Modules\Beneficiario\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	protected $patch_js = [
		'public/js',
		'public/plugins',
		'Modules/Beneficiario/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'Modules/Beneficiario/Assets/css',
	];
}