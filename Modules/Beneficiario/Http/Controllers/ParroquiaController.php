<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\ParroquiaRequest;

//Modelos
use Modules\Beneficiario\Model\parroquia;

class ParroquiaController extends Controller {
	protected $titulo = 'Parroquia';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::parroquia');
	}

	public function getBuscar(Request $request, $id = 0){
		$parroquia = parroquia::find($id);
		
		if ($parroquia){
			return array_merge($parroquia->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(parroquiaRequest $request){
		DB::beginTransaction();
		try{
			$parroquia = parroquia::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(parroquiaRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$parroquia = parroquia::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$parroquia = parroquia::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = parroquia::select([
			'" . implode("', '", $this->columnas) . "'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}