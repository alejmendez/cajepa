<?php
namespace Modules\Beneficiario\Http\Controllers;

class tiempo_transcurrido{
	protected $fecha_ini = '';
	protected $fecha_fin = '';
	protected $fecha_actual = '';
	
	protected $meses = array(31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30); //el primero es la cantidad de dias de diciembre
	
	public function __construct($fecha_ini = false, $fecha_fin = false){
		$this->fecha_actual = date('d/m/Y');
		$this->ini($fecha_ini, $fecha_fin);
	}
	
	public function ini($fecha_ini = false, $fecha_fin = false){
		$this->fecha_ini = $fecha_ini;
		$this->fecha_fin = $fecha_fin === false ? $this->fecha_actual : $fecha_fin;
		
		return $this;
	}
	
	protected function esBisiesto($anio_actual){
		return checkdate(2,29,$anio_actual);
	}
	
	protected function procesar_fecha($fecha){
		if (strpos($fecha, '/') !== false){
			$array_fecha = explode("/", $fecha);
		}else{
			$array_fecha = explode("-", $fecha);
			
			$aux = $array_fecha[2];
			$array_fecha[2] = $array_fecha[0];
			$array_fecha[0] = $aux;
		}
		
		return $array_fecha;
	}
	
	public function calcular(){
		// separamos en partes las fechas
		$array_nacimiento = $this->procesar_fecha($this->fecha_ini);
		$array_actual = $this->procesar_fecha($this->fecha_fin);
		
		$anos =  $array_actual[2] - $array_nacimiento[2]; // calculamos años
		$meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses
		$dias =  $array_actual[0] - $array_nacimiento[0]; // calculamos días
		
		//ajuste de posible negativo en $días
		if ($dias < 0){
			$meses--;
			
			//ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual
			$dias_mes_anterior = $this->meses[$array_actual[1] - 1];
			
			if ($this->esBisiesto($array_actual[2]) && $array_actual[1] == 2){
				$dias_mes_anterior++;
			}
			
			$dias=$dias + $dias_mes_anterior;
			
			if ($dias < 0){
				$meses--;
				if($dias == -1){
					$dias = 30;
				}elseif($dias == -2){
					$dias = 29;
				}
			}
		}
		
		//ajuste de posible negativo en $meses
		if ($meses < 0){
			$anos--;
			$meses = $meses + 12;
		}
		
		return array($anos, $meses, $dias);
	}
}

//$tiempo = new tiempo_transcurrido("26/08/2009");
//var_dump($tiempo->calcular());