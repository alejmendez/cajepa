<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\TipoNominaRequest;

//Modelos
use Modules\Beneficiario\Model\tipo_nomina;

class TipoNominaController extends Controller{

		protected $titulo = 'Tipo Nomina';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public $js = ['TipoNomina'];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::tipoNomina');
	}

	public function getBuscar(Request $request, $id = 0){
		$tipo_nomina = tipo_nomina::find($id);
		
		if ($tipo_nomina){
			return array_merge($tipo_nomina->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(TipoNominaRequest $request){
		DB::beginTransaction();
		try{
			$tipo_nomina = tipo_nomina::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(TipoNominaRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$tipo_nomina = tipo_nomina::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$tipo_nomina = tipo_nomina::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = tipo_nomina::select([
			'tipo_nomina.id','tipo_nomina.nombre'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
    
}
