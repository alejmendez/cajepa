<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\TiposEgresosRequest;

//Modelos
use Modules\Beneficiario\Model\tipo_egresos;

class TiposEgresosController extends Controller{

		protected $titulo = 'Tipo Egresos';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public $js = ['TiposEgresos'];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::tipoEgresos');
	}

	public function getBuscar(Request $request, $id = 0){
		$tipo_egresos = tipo_egresos::find($id);
		
		if ($tipo_egresos){
			return array_merge($tipo_egresos->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(TiposEgresosRequest $request){
		DB::beginTransaction();
		try{

			$tipo_egresos = tipo_egresos::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(TiposEgresosRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$tipo_egresos = tipo_egresos::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$tipo_egresos = tipo_egresos::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = tipo_egresos::select([
			'tipo_egresos.id','tipo_egresos.nombre'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
    
}
