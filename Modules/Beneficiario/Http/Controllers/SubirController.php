<?php

namespace Modules\Beneficiario\Http\Controllers;

use Modules\Beneficiario\Http\Controllers\Controller;

use DB;
use mPDF;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Modules\Admin\Model\data_beneficiario;
use Modules\Beneficiario\Http\Requests\SubirRequest;
use Carbon\Carbon;
use Session;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;



class SubirController extends Controller
{
    public $js = ['subir'];
    public $css = ['usuario'];
    public $titulo='Usuarios';
    public $librerias =[
        'alphanum',
        'maskedinput',
        'datatables',
        'jquery-ui',
        'jquery-ui-timepicker',
        'file-upload',
        'jcrop'
    ];

    public function getIndex() {
    
        return $this->view('beneficiario::subir');
    }

    public function getBuscar(Request $request, $id = 0) {
        $rs = data_beneficiario::find($id);

        if ($rs) {
            return array_merge($rs->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar'),
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function postCrear(SubirRequest $request){
        DB::beginTransaction();
        try {
            $rs = data_beneficiario::create($request->all());
        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();
        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }

    public function putActualizar(SubirRequest $request, $id = 0) {
        DB::beginTransaction();
        try {
            $rs = data_beneficiario::find($id)->update($request->all());
        } catch (Exception $e) {
            DB::rollback();
            return $e->errorInfo[2];
        }

        DB::commit();
        return ['s' => 's', 'msj' => trans('controller.incluir')];
    }
    
    public function deleteEliminar(Request $request, $id = 0) {
        try {
            $rs = data_beneficiario::destroy(intval($id));
        } catch (Exception $e) {
            return $e->errorInfo[2];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function getDatatable() {
        $sql = data_beneficiario::select( 'id', 'fecha', 'monto', 'cedula','nombre','concepto'); 

        return Datatables::of($sql)->setRowId('id')->make(true);
    }
}
