<?php

namespace Modules\Beneficiario\Http\Controllers;
use Modules\Beneficiario\Http\Controllers\Controller;

use DB;
use mPDF;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Modules\Admin\Model\data_beneficiario;
use Modules\Beneficiario\Http\Controllers\tiempo_transcurrido;
use Modules\Beneficiario\Model\banco;
use Modules\Beneficiario\Model\cuenta;
use Modules\Beneficiario\Model\recibo;
use Modules\Beneficiario\Http\Controllers\EnLetras;
use Carbon\Carbon;
use Session;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class EstadoCuentaController extends Controller {
	protected $titulo = 'Estado Cuenta';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
		'jstree',
		'jquery-ui',
		'jquery-ui-timepicker',
	];
	public $js = ['estado_cuenta'];
	public $css = ['estado_cuenta'];
	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
	
		return $this->view('beneficiario::estado_cuenta');
	}
	
    public function getAhorro(Request $request){
		//$conceptos = $this->conceptosCajepa();|
		$cedula = $this->cedula($request->cedu);
		
		$ingresos = $this->db('nphiscon')
		->select(['nphiscon.fecnom','nphiscon.monto'])
		->where('nphiscon.CODEMP',$cedula)
		->orderBy('nphiscon.fecnom', 'DESC')
		->first();

		$monto = $ingresos->monto;

		return $monto;
	
	}
	protected function cedula($cedula){
		$cedula = intval($cedula);
		$cedula = str_pad($cedula, 8, '0', STR_PAD_LEFT);
		$cedula = str_pad($cedula, 16, ' ', STR_PAD_RIGHT);

		return $cedula;
	}

	protected function conceptosCajepa(){
		$rs_conceptos = $this->db('npdefcpt')
			->where('npdefcpt.nomcon','=','CAJA DE AHORRO CAEJPA-GEB.')
			->distinct('npdefcpt.codcon')
			->get();

		$conceptos = [];

		foreach ($rs_conceptos as $key => $value) {
			$conceptos[] = $value->codcon;
		}

		return $conceptos;
	}
	protected function conceptosCajepaPres(){
		$rs_conceptospres = $this->db('npdefcpt')
			->where('npdefcpt.nomcon','=','CUOTA PRESTAMO CAJA DE AHORRO CAEJPA-GEB.')
			->distinct('npdefcpt.codcon')
			->get();

		$conceptospres = [];

		foreach ($rs_conceptospres as $key => $value) {
			$conceptospres[] = $value->codcon;
		}

		return $conceptospres;
	}

	public function postCedulaa($cedula = 0){

		$salida = ['s' => 's', 'msj' => 'datos encontrados', 'cuenta' => ''];

		if($cedula == null ){

			$salida = [ 's' => 'n', 'msj'=> 'ingrese cedula', 'cuenta' => $cuenta];
			return $salida;
				
		}	

		//$cedu = 21578041        ;
		$cedu = $cedula;
		$cedula = $this->cedula($cedu);
		//Session::put('usuario', $cedula);

		$conceptospres = $this->conceptosCajepaPres();
		$conceptos = $this->conceptosCajepa();

        //datos personales
		$empleado = $this->db('nphiscon')
		->select('nphojint.codemp','nphojint.nomemp','nphiscon.codnom','nphiscon.fecnom')
		->join('nphojint','nphiscon.codemp','=','nphojint.codemp')
		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		->where('nphiscon.CODEMP', $cedula)
		->whereIn('npdefcpt.codcon', $conceptos)
		->first();

		$fechaingreso = $empleado->fecnom;
		$fechaing= Carbon::parse($fechaingreso)->format('d/m/y');


		$ingreso = $this->db('nphiscon')
		 ->select('nphiscon.fecnom', 
		 	      'nphiscon.monto',
		 	      'nphiscon.codcon',
		 	       'nphiscon.nomcon'
		 	      )
		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		 ->where('nphiscon.CODEMP', $cedula)
		 ->whereIn('nphiscon.codcon', $conceptos)//'npdefcpt.codcon'
		 ->orderBy('nphiscon.fecnom','DESC')
		 ->distinct()
		 ->get();

		//$totalingreso= $this->sumar($ingreso);//dd(172164.3 + 6641.49 + 6641.49);//6641.49
		//$totalingre = $this->sumar($ingreso);
		//$int = intval($totalingre); 
		//$totalingreso= intval($totalingre)- 26565.96;
		$totalingreso= 172164.3- 26565.96;
        


		$retiro = DB::table('data_beneficiario')
			->select('monto','fecha','concepto')
			->where('cedula',$cedula)
			->where('concepto', 'LIKE','RETIRO%')
			->orderBy('fecha','DESC')
			->first(); 

	    $fech=$retiro->fecha;
	    $fech_= Carbon::parse($fech)->format('d/m/Y');

		$sumaretiro = DB::table('data_beneficiario')
			->select('monto','concepto', 'fecha')
			->where('cedula',$cedula)
			->where('concepto', 'LIKE','RETIRO%')
			->get();

		$totalsumaretiro= $this->sumar($sumaretiro);

		$prestamos = DB::table('data_beneficiario')
			->select('monto','fecha')
			->where('cedula', $cedula)
			->where('concepto', '=','PRESTAMOS')
			->orderBy('fecha','DESC')
			->first();

		$prestamostotal = DB::table('data_beneficiario')
			->select('monto')
			->where('cedula',$cedula )
			->where('concepto', '=','PRESTAMOS')
			->get();

		$totalprestamostotal= $this->sumar($prestamostotal);

		$n = $this->db('nptippre')
		->select('nphispre.monpre','nphispre.saldo','nphispre.fechispre','nphispre.CODEMP')
		->join('nphispre','nphispre.codtippre','=','nptippre.codtippre')
		->join('npdefcpt','npdefcpt.codcon','=','nptippre.codcon')
		->where('nphispre.CODEMP', $cedula)
		->whereIn('npdefcpt.codcon',$conceptospres )
		->orderBy('nphispre.fechispre','DESC')
		->first();

		$s = $this->db('nptippre')
		->select('*')
		->join('nphispre','nphispre.codtippre','=','nptippre.codtippre')
		->join('npdefcpt','npdefcpt.codcon','=','nptippre.codcon')
		->where('nphispre.CODEMP', $cedula)
		->whereIn('npdefcpt.codcon',$conceptospres )
		->orderBy('nphispre.fechispre','DESC')

		->get();

		$conjemplo = $this->db('NPHISCON')
		->select('*')
		->where('NPHISCON.codemp', 'like',$cedula.'%')
		->where('nphiscon.nomcon', 'like','%CAJA DE AHORRO%' )
		->where('nphiscon.nomcon', 'like','%PRESTAMO%' )
		->orderBy('nphiscon.fecnom', 'DESC')
		->get();



		$fechaingreso = $this->db('nphiscon')
		 ->select('nphiscon.fecnom')
		 ->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		 ->where('nphiscon.CODEMP', $cedula)
		 ->whereIn('npdefcpt.codcon', $conceptos)
		 ->orderBy('nphiscon.fecnom', 'DESC')
		 ->first();

		

		if (!$n || is_null($n)) {
			$n = [
			'monpre'=>'0',
			'saldo'=>'0',
			'fechispre'=>'0'
			];
		}
		if (!$prestamos){
			$prestamos = [
			'monto'=>'0',
			'fecha'=>'Vacio'
			];
		}
		if (!$retiro){
			$retiro = [
			'concepto'=>'0',
			'monto'=>'0',
			'fecha'=>'Vacio'
			];
		}
		if (!$prestamostotal){
			$prestamostotal = [
			'monto'=>'0'
			];
		}

		$periodo = $this->contar($fechaingreso);
		if ($periodo[0] != 0 || $periodo[1] >= 2) {
			$estatus =  'inactivo';
		}else{
			$estatus = 'Activo';
		}

		$resta =$totalingreso - $totalsumaretiro - $totalprestamostotal;
		$ahorro = $resta / 2;
		$resultadototal = $resta * 0.8;

		////geneeee
		$monto = $this->db('nphiscon')
		->select(['nphiscon.fecnom','nphiscon.monto'])
		->where('nphiscon.CODEMP',$cedula)
		->orderBy('nphiscon.fecnom', 'DESC')
		->first();

		$aporte = $totalingreso  / 2;
        $porcenta_80 = $totalingreso * 0.8; // 80 porciento del total de ahorros
        $porcenta_20 = $totalingreso * 0.2;

		//->get();

		return $r = [
		'empleado'            =>  $empleado,
		'n'                   =>  $n,
		'ingreso'             =>  $ingreso,
		'prestamos'           =>  $prestamos,
		'retiro'	          =>  $retiro,
		'aporte'              =>  $aporte,
		'porcenta_80'         =>  $porcenta_80,
		'porcenta_20'         =>  $porcenta_20,
		'totalingreso'        =>  $totalingreso,
		'totalsumaretiro'     =>  $totalsumaretiro,
		'totalprestamostotal' =>  $totalprestamostotal,
		'fech'                =>  $fech_,
		'salida'              => $salida
		];


      

	}
	
	protected function db($tabla = ''){
		$db = DB::connection('sima');
		
		if ($tabla !== ''){
			$db = $db->table($tabla);
		}

		return $db;
	}

	public function getBuscar(Request $request, $id = 0){
		$beneficiario = beneficiario::find($id);

		$nomina = DB::table('codigo_nomina')->select('codigo','id')->where('id', $beneficiario->nomina_id)->get();

		foreach ($nomina as $n) {
			
		$condicional= DB::table('codigo_condicional')->select('codigo','id', 'codigo_nomina_id')->where('codigo_nomina_id', $n->id)->get();
		}

		
		if ($beneficiario){
			return array_merge($beneficiario->toArray(), [
				'condicional'=>$condicional,
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	   }
		/*
			public function postCrear(estado_cuenta_ingresosRequest $request){
				DB::beginTransaction();
				try{
					$estado_cuenta_ingresos = estado_cuenta_ingresos::create($request->all());
				}catch(Exception $e){
					DB::rollback();
					return $e->errorInfo[2];
				}
				DB::commit();

				return ['s' => 's', 'msj' => trans('controller.incluir')];
			}*/

	public function putActualizar(estado_cuenta_ingresosRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$estado_cuenta_ingresos = estado_cuenta_ingresos::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

    public function getDatatable(Request $request){ 	
        $conceptos = $this->conceptosCajepa();
		$cedula = $this->cedula($request->cedula);
		
		$ingresos = $this->db('nphiscon')
		->select(['nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon'])
		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		->where('nphiscon.CODEMP',$cedula)
		->whereIn('npdefcpt.codcon', $conceptos);
	
	    return Datatables::of($ingresos)->make(true); 
    }

	public function getDatatables(Request $request){
		$conceptospres = $this->conceptosCajepaPres();
		$cedula = $this->cedula($request->cedula);

		$egresos = $this->db('nphiscon')
		->select(['nphiscon.fecnom','nphiscon.monto','nphiscon.codcon','nphiscon.nomcon'])
		->join('npdefcpt','nphiscon.codcon','=','npdefcpt.codcon')
		->where('nphiscon.CODEMP',$cedula)
		->whereIn('npdefcpt.codcon',$conceptospres );

	    return Datatables::of($egresos)->make(true); 
	}
	public function getDatatablerp(Request $request){
		$cedula = $this->cedula($request->cedula);

		$retpres = DB::table('data_beneficiario')
			->select('monto','fecha','concepto')
			->where('cedula',$cedula);

	    return Datatables::of($retpres)->make(true); 
	}
	public function contar($fecha)
	{
		$fecha = Carbon::parse($fecha->fecnom)->format('d/m/Y');
		$periodo = new tiempo_transcurrido;
 		$periodo->ini($fecha);		
       		
       	return $periodo->calcular();
        
	}

	public function sumar($valor){
		 $total = 0;
		 foreach ($valor as $key ) {
            $total+=$key->monto;
        //retiro
		 }
		 return $total;
	}


	public function getPdf(Request $request){

        $cedula = $request->cedu_consulta;
		$gasto = $request->gasto_consulta;
        
        if ($gasto == null || $cedula == null) {

        	$salida = ['s' => 'n', 'msj' => 'ingrese todos los datos', 'cuenta' => ''];
        	return $salida;
        }  

		$mpdf = new mPDF();
		$url='beneficiario::pdf.reporte';
        $consulta = $this->postCedulaa($cedula);
        //dd($consulta['empleado']->nomemp);
        $aporte = $consulta['totalingreso'] /   2; // se divide entre dos porque la mitad es  retenciones y la mitad aporte
        $totalahorros = $consulta['totalingreso'];//total de ahorro suma total de ahorros retenciones mas apore
        $porcenta_80 = $totalahorros * 0.8; // 80 porciento del total de ahorros
        //dd($porcenta_80);
        $porcenta_20 = $totalahorros * 0.2;


		$retiroparcial = $this->sumaretiro($consulta['totalingreso'],$consulta['totalsumaretiro'], $consulta['totalprestamostotal'], $gasto);
		//dd($porcenta_80);
		//$totalingreso- $totalsumaretiro - $totalprestamostota - $gasto;

        $parcial_80 = $retiroparcial * 0.8;
        $parcial_20 = $retiroparcial * 0.2;
        
        $ahorro = $consulta['totalingreso'] / 2;
        $neto =$this->neto($consulta['totalsumaretiro'],$porcenta_80, $parcial_80);
 
        
		$mpdf->WriteHTML(view($url, [
		  	'aporte'  => $aporte,
		  	'totalahorros'   =>  number_format($totalahorros,2, ',', '.'),
		  	'empleado'=> $consulta['empleado'],
		  	'por_80'  =>  number_format($porcenta_80, 2, ',', '.'),
		  	'por_20'  => number_format($porcenta_20,2, ',', '.'),
		  	'gasto'  => $gasto,
		  	'prestamos'=>$consulta['totalprestamostotal'],
		  	'retiros'=>number_format($consulta['totalsumaretiro'],2, ',', '.'),
		  	'retiroparcial' => number_format($retiroparcial,2, ',', '.'),
		  	'parcial_80'=>number_format($parcial_80,2, ',', '.'),
		  	'parcial_20'=>number_format($parcial_20,2, ',', '.'),
		  	'gasto'=> $gasto,
		  	'neto'=> number_format($neto,2, ',', '.')

		])->render());
	      
	    $mpdf->Output();
	     exit;

       
        /*
		$mpdf = new mPDF()

        $url='beneficiario::pdf.reporte';

        $mpdf->WriteHTML(view($url)->render());

	    $mpdf->Output();
	   // exit;*/

	}

	public function getReporte(Request $request){
        $cedula = $request->cedu_reporte;
        $gasto  = $request->gasto_reporte;
        $cheque = $request->cheque;
        $banco  = $request->banco_id;
        $cuenta = $request->cuenta_id;
        $cuotas = $request->cuotas;
        $fecha  = $request->fecha_modal;
        $tipo  = $request->tipo;
        $fech = $this->fec_español( $fecha);

        $fiadores =0;

        if($request->fia == "si"){

           $fiadores = $request->fiador_1 + $request->fiador_2 + $request->fiador_3 + $request->fiador_4;

        }

        $extras= 0;

        if ($request->extras != null) {

        	$extras= $request->extras;
        }

       
		$mpdf = new mPDF();

        $url='beneficiario::pdf.reporte2';

        $consulta = $this->postCedulaa($cedula);
        $aporte = $consulta['totalingreso'] /   2; // se divide entre dos porque la mitad es  retenciones y la mitad aporte
        $totalahorros = $consulta['totalingreso'];//total de ahorro suma total de ahorros retenciones mas apore
        $porcenta_80 = $totalahorros * 0.8 + $fiadores ; // 80 porciento del total de ahorros 
        $porcenta_20 = $totalahorros * 0.2;
        $retiroparcial = $this->sumaretiro($totalahorros, $consulta['totalsumaretiro'], $consulta['totalprestamostotal'], $gasto, $extras);
        $parcial_80 = $retiroparcial * 0.8;
        $parcial_20 = $retiroparcial * 0.2;

        $neto = 0;
       
        if ($tipo == 'total') {

        	$neto = $retiroparcial;

        }
        else{

        	$neto =$this->neto($consulta['totalsumaretiro'],$porcenta_80, $parcial_80 );

        }

        $interes = 0;

        if ($tipo == 'prestamo') {

        	$interes= $this->intereses($cuotas, $neto);
        	$neto = $retiroparcial - $interes;
        	//dd($interes);

        }
   
      
        $neto_80    = $this->neto($consulta['totalsumaretiro'],$porcenta_80, $parcial_80 );
        

        

        $restante= $this->neto($consulta['totalsumaretiro'],$porcenta_20, $parcial_20 );

        $banco_nombre = banco::select(['banco.nombre as nombre'])
        ->where('id', $banco)
        ->first();

        $cuenta_numero = cuenta::select(['cuenta.numero as numero'])
        ->where('id', $cuenta)
        ->first();
     
        
        
     
        $nLetras      = new EnLetras();
        $monto_letras = $nLetras->ValorEnLetras($neto);
        //$neto =$this->neto($consulta['totalsumaretiro'],$porcenta_80, $parcial_80 );

        $ahorro = $consulta['totalingreso'] / 2;
		$mpdf->WriteHTML(view($url, [
			'nombre'         => $consulta['empleado']->nomemp,
		  	'totalahorros'   => number_format($totalahorros,2, ',', '.'),
		  	'prestamos'      =>$consulta['totalprestamostotal'],
		  	'banco'          => $banco_nombre->nombre,
		  	'cuenta'         => $cuenta_numero->numero,
		  	'cheque'         => $cheque,
		  	'monto_letras'   => $monto_letras,
		  	'fecha'          => $fech,
		  	'neto'           => number_format($neto,2, ',', '.'),
		  	'interes'        => number_format($interes,2, ',', '.'),
		  	'restante'       => number_format($parcial_20,2, ',', '.'),
		  	'neto_80'        => number_format($neto_80,2, ',', '.')

		])->render());


	      
	    $mpdf->Output();
	     exit;
	}

	public function sumaretiro($porcenta_80, $totalsumaretiro ,$totalprestamostota, $gasto){
		if ($gasto = null) {
			$gasto = 0;
		}
		
        $retiroparcial =  $porcenta_80- $totalsumaretiro - $totalprestamostota - $gasto;
        //dd($retiroparcial);

        if ($retiroparcial  >= '30000') {
        	$retiroparcial = $retiroparcial - 0.80;	
        }

        return $retiroparcial; 

	}

	public function neto($retiro, $porcenta_80, $parcial_80){
		if ($retiro = 0) {

        	$neto= $porcenta_80;

        }
        
        $neto= $parcial_80;

        return $neto;
        
	}

	public function getMigracion(){

		Schema::create('recibo', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('cheque', 50)->unique();
            $table->integer('banco_id')->unsigned();
            $table->string('total_ahorros');
            $table->string('nombre');
            $table->integer('cedula');
            $table->string('prestamos');
            $table->string('interes');
            $table->string('retiro_parcial');
            $table->string('prestamos_80');
            $table->string('ahorro_restantes');
            $table->string('fecha');
             $table->integer('año');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('banco_id')->references('id')->on('banco')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('data_beneficiario_id')->references('id')->on('data_beneficiario')->onDelete('cascade')->onUpdate('cascade');
    	});

        return 'migracion' . time();
    }

   public function banco(){
   	return  banco::pluck('nombre', 'id');
   }

   public function getCuenta(Request $request){
   	$banco = $request->banco;

   	$query = banco::select('banco.nombre as banco',
   							'banco.cuenta as cuenta'
   		                   )
   	->where('banco.id', $banco)
   	->get()
   	->toArray();
     $cuenta = $query[0]['cuenta'];

     return ['cuenta'=> $cuenta,];


   }


   public function getModal(Request $request){

   	$fecha = strftime("%A %d de %B del %Y");
   	$año = date('Y');
   	$consulta = $this->postCedulaa($request->cedu);
   	$cedula=   $this->cedula($request->cedu);
   	$totalahorros = $consulta['totalingreso'];
   	$prestamos    = $consulta['totalprestamostotal'];
   	$gasto        = $request->gasto;

   	$porcenta_80 = $totalahorros * 0.8; // 80 porciento del total de ahorros
    $porcenta_20 = $totalahorros * 0.2;
    $retiroparcial = $this->sumaretiro($consulta['totalsumaretiro'], $consulta['totalingreso'], $consulta['totalprestamostotal'], $gasto);
    $parcial_80 = $retiroparcial * 0.8;
    $parcial_20 = $retiroparcial * 0.2;
    $neto =$this->neto($consulta['totalsumaretiro'],$porcenta_80, $parcial_80 );
    $restante= $this->neto($consulta['totalsumaretiro'],$porcenta_20, $parcial_20 );

	$empleado = $this->db('nphojint')
		->select('nphojint.codemp','nphojint.nomemp')
		->where('nphojint.codemp', $cedula)
		->first();

   $nombre= $empleado->nomemp;
	$guardar= recibo::create([
		    'cheque'   =>  $request->cheque, 
	   		'banco_id'     => $request->banco ,
	   		'total_ahorros'=>$neto,
	   		'prestamos'=>$prestamos,
	        'retiro_parcial'=> number_format($retiroparcial, 2, ',', '.'),
	        'ahorro_restantes'=>$restante,
	   		'fecha'=>$fecha,
	   		'año'=>  $año ,
	   		'nombre'=>$nombre,
	   		'cedula'=> $request->cedu
	]);



	return ['s' => 's', 'msj' => trans('controller.incluir')];
   }

   public function getNumero(Request $request, $id = 0) {
   		$salida = ['s' => 'n', 'msj' => 'no se encontraron cuentas', 'cuenta' => ''];
		
		$cuenta =  cuenta::where('banco_id', $id)->lists('numero','id');
			if($cuenta != null){
				$salida = [ 's' => 's', 'msj'=> 'cuentas encontradas', 'cuenta' => $cuenta];
				
			}		
		return $salida;
	}

	public function getCajepa(Request $request){
       
		$mpdf = new mPDF();
		$url='beneficiario::pdf.reporte2';
		$mpdf->WriteHTML(view($url, [])->render());
	      
	    $mpdf->Output();
	     exit;
	}


	public function intereses($cuotas, $neto){
	 	if ($cuotas <= 24) {
	 		$cuot= "12";
	 	}
	 	else{
	 		$cuot = $cuotas;
	 	}

	 	$inter = $cuot * $neto;
	 	$interes = $inter /100;

	 	return $interes;
	}

	public function fec_español($fecha){

		$numeroDia = date('d', strtotime($fecha));
        $dia = date('l', strtotime($fecha));
  		$mes = date('F', strtotime($fecha));
  		$anio = date('Y', strtotime($fecha));

  		$dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
  		$dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  		$nombredia = str_replace($dias_EN, $dias_ES, $dia);
  		$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  		$meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
 		$nombreMes = str_replace($meses_EN, $meses_ES, $mes);

 		  return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
        //dd($nombreMes);


	}

}

