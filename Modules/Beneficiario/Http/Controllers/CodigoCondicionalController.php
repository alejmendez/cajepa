<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\CodigoCondicionalRequest;

//Modelos
use Modules\Beneficiario\Model\codigo_condicional;
use Modules\Beneficiario\Model\codigo_nomina;

class CodigoCondicionalController extends Controller {
	protected $titulo = 'Codigo Condicional';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public $js=[

		'CodigoCondicional'
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {

		return $this->view('beneficiario::CodigoCondicional');
	}

	public function getBuscar(Request $request, $id = 0){
		$codigo_condicional = codigo_condicional::find($id);
		
		if ($codigo_condicional){
			return array_merge($codigo_condicional->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(CodigoCondicionalRequest $request){
		DB::beginTransaction();
		try{
			$codigo_condicional = codigo_condicional::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(CodigoCondicionalRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$codigo_condicional = codigo_condicional::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$codigo_condicional = codigo_condicional::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}


 	public function codigo_nominas(){

 		return codigo_nomina::lists('codigo', 'id');

 	}	

	public function getDatatable(){
		$sql = codigo_condicional::select(['codigo_condicional.id',
			'codigo_condicional.nombre','codigo_nomina.nombre as nombre_nomina' ,'codigo_nomina.codigo'
		])
		->join('codigo_nomina','codigo_nomina.id','=','codigo_condicional.codigo_nomina_id');


	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}