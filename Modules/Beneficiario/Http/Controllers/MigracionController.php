<?php

namespace Modules\Beneficiario\Http\Controllers;
use Modules\Beneficiario\Http\Controllers\Controller;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigracionController extends Controller {

	public $js = [
        'migracion.js'

	];

	public function index(){
		Schema::create('recibo', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('cheque', 50)->unique();
            $table->integer('banco_id')->unsigned()->nullable();
            $table->integer('data_beneficiario_id')->unsigned()->nullable();
            $table->string('total_ahorros');
            $table->string('prestamos');
            $table->string('interes');
            $table->string('retiro_parcial');
            $table->string('prestamos');
            $table->string('ahorro_restantes');
            $table->date('fecha');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('banco_id')->references('id')->on('banco')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('data_beneficiario_id')->references('id')->on('data_beneficiario')->onDelete('cascade')->onUpdate('cascade');
    	});
    }
}