<?php

namespace Modules\Beneficiario\Http\Controllers;
use Modules\Beneficiario\Http\Controllers\Controller;

use Validator;
use DB;
use Image;
use Excel;
use Carbon\Carbon;
use Session;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Modules\Beneficiario\Model\Imagen;
use Modules\Beneficiario\Model\data_beneficiario;



class CargarController extends Controller {
	protected $titulo = 'Cargar';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
        'jquery-ui',
        'file-upload',
        'jcrop',
        'cargar'
	];	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
	
	
		return $this->view('beneficiario::cargar');
	}
	public function postCrear(Request $request){

                set_time_limit(0);
                DB::beginTransaction();
                try {
                        
                    

                        $mimes = [
                        'text/csv',
                        'application/vnd.ms-excel',
                        'application/vnd.oasis.opendocument.spreadsheet',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        ];

                        $ruta = public_path('archivos/productos/');
                        $archivo = $request->file('archivo');

                        $mime = $archivo->getClientMimeType();

                        /*if (!in_array($mime, $mimes)) {
                                return "Error de Validación, la extension el archivo cargado debe ser: xls,xlsx,csv";
                        }*/
                        
                        do {
                                $nombre_archivo = str_random(20) . '.' . $archivo->getClientOriginalExtension();
                        } while (is_file($ruta . $nombre_archivo));
                                
                        $archivo->move($ruta, $nombre_archivo);

                       // chmod($ruta . $nombre_archivo, 0777);

                        $excel = Excel::selectSheetsByIndex(0)->load($ruta . $nombre_archivo)->get();
                       // dd($excel);
                      

                         foreach ($excel as $key => $value) {
                                     

                                  if(empty($value->fecha) || empty($value->cedula) || empty($value->nombre) || empty($value->monto) || empty($value->concepto)){

                                      return "Error, revisa el excel";
                                  }else{

                                      $fecha= Carbon::parse($value->fecha)->format("Y-m-d");

                                       // $monto = floatval(str_replace(',', '.', $value->monto));
                                        //$cedula = floatval(str_replace(',', '.', $value->cedula));
                                
                                         data_beneficiario::create([

                                                'fecha'                                         =>        $fecha,
                                                'cedula'                                        =>        $value->cedula,
                                                'nombre'                                        =>        $value->nombre,
                                                'monto'                                         =>        $value->monto,
                                                'concepto'                                      =>        $value->concepto
                                        ]);
                                  }
                                        
                                 }        

                } catch (Exception $e) {
                        DB::rollback();
                        return $e->errorInfo[2];
                }


                DB::commit();
                Session::put('exito', "exito");

                return redirect()->route('cargar');

                //return ['s' => 's', 'msj' => trans('controller.incluir')];
        }

 /* public function postArchivos(Request $request) {
    $validator = Validator::make($request->all(), [
            'files.*' => ['required', 'mimes:jpeg,jpg,png,xlsx'],
        ]);

        if ($validator->fails()) {
            return 'Error de Validacion';
        }

    $files = $request->file('files');
    $url = $this->ruta();
    $url = substr($url, 0, strlen($url) - 6);

    $rutaFecha = $this->getRuta();
    $ruta = public_path('/productos/' . $rutaFecha);
    
    $respuesta = array( 
      'files' => array(),
    );

    foreach ($files as $file) {

      $nombre = $this->random_string(). ".".$file->getClientOriginalExtension();

        $archivo= $file;
        $ruta= public_path('archivos/productos/');

        $id = str_replace('/', '-', $nombre);

        $respuesta['files'][] = [
          'id' => $id,
          'name' => $nombre,
          'size' => $file->getSize(),
          'type' => $file->getMimeType(),
          //'url' => url('imagen/small/' . $rutaFecha . $nombre_archivo),
          'url' => url('public/archivos/productos/'),
          'thumbnailUrl' => url('public/archivos/productos/'. $nombre),
          'deleteType' => 'DELETE',
          'deleteUrl' => url($url . '/eliminarimagen/' . $id),
           'data' => [
            'cordenadas' => [],
            'leyenda' => '',
            'descripcion' => ''
          ]
        ];
        $archivo->move($ruta, $nombre);
    }

    return $respuesta;
  }
  protected function getRuta()
  {
    return date('Y') . '/' . date('m') . '/';
  }
    protected function random_string($length = 20) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
    }

    return $key;
  }*/
}