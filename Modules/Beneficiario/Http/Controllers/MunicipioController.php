<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\MunicipioRequest;

//Modelos
use Modules\Beneficiario\Model\municipio;
use Modules\Beneficiario\Model\estado;

class MunicipioController extends Controller {
	protected $titulo = 'Municipio';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::municipio');
	}

	public function getBuscar(Request $request, $id = 0){
		$municipio = municipio::find($id);
		
		if ($municipio){
			return array_merge($municipio->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(municipioRequest $request){
		DB::beginTransaction();
		try{
			$municipio = municipio::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(municipioRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$municipio = municipio::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$municipio = municipio::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}


 	public function estados(){

 		return estado::lists('nombre', 'id');


 	}	

	public function getDatatable(){
		$sql = municipio::select([
			'municipio.id','municipio.nombre', 'estado.nombre as estado' 
		])->join('estado', 'estado.id', '=', 'municipio.estado_id');

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}