<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\EstadoRequest;

//Modelos
use Modules\Beneficiario\Model\estado;

class EstadoController extends Controller {
	protected $titulo = 'Estado';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::estado');
	}

	public function getBuscar(Request $request, $id = 0){
		$estado = estado::find($id);
		
		if ($estado){
			return array_merge($estado->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(estadoRequest $request){
		DB::beginTransaction();
		try{
			$estado = estado::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(estadoRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$estado = estado::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$estado = estado::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = estado::select([
			'" . implode("', '", $this->columnas) . "'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}