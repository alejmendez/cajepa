<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\EstatusRequest;

//Modelos
use Modules\Beneficiario\Model\estatus;

class EstatusController extends Controller{

		protected $titulo = 'Estatus';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public $js = ['Estatus'];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::estatus');
	}

	public function getBuscar(Request $request, $id = 0){
		$estatus = estatus::find($id);
		
		if ($estatus){
			return array_merge($estatus->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(EstatusRequest $request){
		DB::beginTransaction();
		try{
			$estatus = estatus::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(EstatusRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$estatus = estatus::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$estatus = estatus::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = estatus::select([
			'estatus.id','estatus.nombre'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
    
}
