<?php 

namespace Modules\Beneficiario\Http\Controllers;
use Modules\Beneficiario\Http\Controllers\Controller;

//use Modules\Pagina\Http\Controllers\tiempo_transcurrido;
//use Carbon\Carbon;
//use DB;
//use Illuminate\Http\Request;
//use Session;

//use Modules\Admin\Http\Controllers\BloqueosController;


class reciboController extends Controller {
	
	public $css =[
		'slider',
		'magnific-popup',
		'recibo_pago'
	];
	public $js =[
		'recibo_pago',
	];

	public $librerias = [
		'jquery-ui',
		'jquery-ui-timepicker',
	];

	   public function __construct(){
        parent::__construct();
    }

    public function getIndex() {
    
    
        return $this->view('beneficiario::recikkkbo');
    }

 // protected function cedula($cedula){
 //                $cedula = intval($cedula);
 //                $cedula = str_pad($cedula, 8, '0', STR_PAD_LEFT);
 //                $cedula = str_pad($cedula, 16, ' ', STR_PAD_RIGHT);
 //                return $cedula;
 //    }

 // public function postFecha(Request $request){
 //        $consulta = $this->postConsultar($request->fecha, Session::get('usuario'));
 //        return $consulta;
 // }

	 public function getRecibo(Request $request){


	        $consulta = $this->postConsultar(Session::get('fecha'), Session::get('usuario'));
	        $hasta = Carbon::parse(Session::get('fecha'))->format('d/m/y');

	        $completar = Carbon::parse(Session::get('fecha'))->format('m/y');
	        $desde = $consulta['recibos']['frecal'].'/'.$completar;

	        /*pagina::pdf.recibo*/
	        $html = view('beneficiario::reporte/pdf', [
	            'recibos' => [
	                'nombre'=>      $consulta['recibos']['nombre'],
	                'cedula'=>      $consulta['recibos']['cedula'],
	                'ingreso'=>     $consulta['recibos']['ingreso'],
	                'cargo'=>       $consulta['recibos']['cargo'],
	                'codnom'=>      $consulta['recibos']['codnom'],
	                'codCargo'=>    $consulta['recibos']['codCargo'],
	                'salario'=>     $consulta['recibos']['salario'],
	                'nombreNomina'=>$consulta['recibos']['nombreNomina'],
	                'nombreBanco'=> $consulta['recibos']['nombreBanco'],
	                'codNiv'=>      $consulta['recibos']['codNiv'],
	                'desNiv'=>      $consulta['recibos']['desNiv'],
	                'neto'=>        $consulta['recibos']['neto'],
	                'nroRecibo'=>   $consulta['recibos']['nroRecibo'],
	                'otraInst'=>    $consulta['recibos']['otraInst'],
	                'desde'=>       $desde,
	                'hasta'=>       $hasta,
	            ],
	                'conceptos'=> $consulta['conceptos'],

	                'tiempoServicio'=>[$consulta['tiempoServicio']['años'],$consulta['tiempoServicio']['mes']],
	                'html'  => $request->has('html')
	            ])->render();


	        if ($request->has('html')){
	            return $html;
	        }
	        $pdf =  new \mPDF('letter');
	        $pdf->WriteHTML($html);
	        $pdf->Output();
	 }

 // public function frecuenciaPago($tipo){
 //        $fecha_m = explode("/", Session::get('fecha'));

 //        if ($tipo == 'Q') {
 //            if ($fecha_m[0] == 15) {
 //                $fecha_m[0] = '01';
 //            } elseif ($fecha_m[0] > 27) {
 //                $fecha_m[0] = '16';
 //            }

 //        } elseif ($tipo == 'S') {
 //            $fecha = $this->sumar_dias($fecha, 6, 0);
 //            $fecha_m = explode("/", $fecha);
 //        } elseif ($tipo == 'M') {
 //            $fecha_m[0] = '01';
 //        }

 //        //$fecha_m[1] = strtoupper($meses[intval($fecha_m[1]) - 1]);

 //        return implode("/", $fecha_m);
 //    }
 //    protected function sumar_dias($fecha, $dias, $suma = 1) {
 //        $exp = explode("/", $fecha);

 //        $mon = $exp[1];
 //        $day = (strlen($exp[2]) > 2) ? $exp[0] : $exp[2];
 //        $year = (strlen($exp[0]) < 4) ? $exp[2] : $exp[0];

 //        $mktime = mktime(0, 0, 0, $mon, $day, $year);

 //        if ($suma == 1) {
 //            $rs = $mktime + ($dias * 24 * 60 * 60);
 //        } else {
 //            $rs = $mktime - ($dias * 24 * 60 * 60);
 //        }

 //        $fecha = (strlen($exp[2]) > 2) ? date("d/m/Y", $rs) : date("Y/m/d", $rs);
 //        return $fecha;
 //    }

	// public function postConsultar($fecha, $cedula){

	// 	$fecha = Carbon::parse($fecha)->format('y-m-d');
 //        $cedula = $this->cedula($cedula);
 //        $fecha =  "20".$fecha;

 //        Session::put('fecha', $fecha);

 //            $conceptos = DB::connection('sima') 
 //                    ->table('NPHISCON')
 //                    ->select(   'NPHISCON.CODEMP',
 //                                'NPHISCON.FECNOM',
 //                                'NPDEFCPT.CODCON', 
 //                                'NPDEFCPT.NOMCON',
 //                                'NPHISCON.MONTO',
 //                                'NPDEFCPT.OPECON')
 //                    ->join('NPDEFCPT', 'NPHISCON.CODCON', '=', 'NPDEFCPT.CODCON')
 //                    ->join('NPHOJINT', 'NPHISCON.CODEMP', '=', 'NPHOJINT.CODEMP')
 //                    ->where('NPHISCON.CODEMP', $cedula)
 //                    ->where('NPHISCON.FECNOM','like', $fecha.'%')
 //                    ->get(); 

 //            if ($conceptos->toArray() == []) {
 //            	return $rs=['s'=>'n','msj'=>'La Fecha que selecciono no corresponde a una fecha de pago.'];
 //            }
            
 
 //            $datosRecibo = DB::connection('sima') 
 //                    ->table('NPHISCON')
 //                    ->select(   'NPHISCON.CODEMP',
 //                                'NPHISCON.NOMEMP',
 //                                'NPHISCON.NUMREC',  
 //                                'NPHOJINT.FECING',
 //                                'NPHOJINT.ANOOTRINST',
 //                                'NPHISCON.NOMCAR',
 //                                'NPHISCON.CODNOM',
 //                                'NPHISCON.CODCAR',
 //                                'NPHISCON.NOMBAN',
 //                                'NPASICONEMP.MONTO',
 //                                'NPNOMINA.NOMNOM',
 //                                'NPNOMINA.FRECAL',
 //                                'NPESTORG.CODNIV',
 //                                'NPESTORG.DESNIV')
 //                    ->join('NPHOJINT', 'NPHISCON.CODEMP', '=', 'NPHOJINT.CODEMP')
 //                    ->join('NPASICONEMP', 'NPHISCON.CODEMP', '=', 'NPASICONEMP.CODEMP')
 //                    ->join('NPNOMINA', 'NPNOMINA.CODNOM','=', 'NPHISCON.CODNOM')
 //                    ->join('NPESTORG', 'NPESTORG.CODNIV','=', 'NPHOJINT.CODNIV')
 //                    ->where('NPHISCON.CODEMP','like',$cedula)
 //                    ->orWhere('NPHISCON.CODEMP', $cedula)
 //                    ->orderBy('NPHISCON.FECNOM', 'DESC')
 //                    ->first();

 //            $bloqueo = new BloqueosController;

 //            $activarBloqueo = $bloqueo->bloquear('ReciboDePago', Session::get('fecha'), $datosRecibo->codnom);

 //            if ($activarBloqueo) {
 //                return $rs=['s'=>'n','msj'=>'La Fecha que selecciono no corresponde a una fecha de pago.'];
 //            }
            
 //            $frecuencia = $this->frecuenciaPago($datosRecibo->frecal);

 //            $suma=0;
 //            $resta=0;
 //            $neto = 0;
 //            foreach ($conceptos as $key) {
 //                if ($key->opecon == "A") {
 //                   $suma += $key->monto;
 //                }
 //                if ($key->opecon == "D") {
 //                    $resta += $key->monto;
 //                }
 //            }

 //            $neto = $suma - $resta;
         
 //            $periodo = new tiempo_transcurrido;
 //            $periodo->ini($datosRecibo->fecing);

 //            $tiempo_servicio = $periodo->calcular();

 //           return $rs=[
 //                    'recibos' => [
 //                        'nombre'=> $datosRecibo->nomemp,
 //                        'cedula'=> $datosRecibo->codemp,
 //                        'ingreso'=> $datosRecibo->fecing,
 //                        'cargo'=> $datosRecibo->nomcar,
 //                        'codnom'=> $datosRecibo->codnom,
 //                        'codCargo'=> $datosRecibo->codcar,
 //                        'salario'=> $datosRecibo->monto,
 //                        'nombreNomina'=> $datosRecibo->nomnom,
 //                        'nombreBanco'=> $datosRecibo->nomban,
 //                        'codNiv'=> $datosRecibo->codniv,
 //                        'desNiv'=> $datosRecibo->desniv,
 //                        'neto'=> $neto,
 //                        'nroRecibo'=>$datosRecibo->numrec,
 //                        'otraInst'=>$datosRecibo->anootrinst,
 //                        'frecal'=>$frecuencia,
 //                    ],
 //                    'conceptos'=> $conceptos,

 //                    'tiempoServicio'=>[
 //                        'años'=> $tiempo_servicio[0],
 //                        'mes' => $tiempo_servicio[1],
 //                    ],
 //            ];
	// }
}