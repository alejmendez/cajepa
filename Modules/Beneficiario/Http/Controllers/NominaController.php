<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\NominaRequest;

//Modelos
use Modules\Beneficiario\Model\nomina;

class NominaController extends Controller {
	protected $titulo = 'Nomina';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::nomina');
	}

	public function getBuscar(Request $request, $id = 0){
		$nomina = nomina::find($id);
		
		if ($nomina){
			return array_merge($nomina->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(nominaRequest $request){
		DB::beginTransaction();
		try{
			$nomina = nomina::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(nominaRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$nomina = nomina::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$nomina = nomina::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}


 	public function estados(){

 		return estado::lists('nombre', 'id');


 	}	

	public function getDatatable(){
		$sql = nomina::select([
			'nombre'
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
}