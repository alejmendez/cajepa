<?php

namespace Modules\Beneficiario\Http\Controllers;

//Controlador Padre
use Modules\Beneficiario\Http\Controllers\Controller;

//Dependencias
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

//Request
use Modules\Beneficiario\Http\Requests\CodigoNominaRequest;

//Modelos
use Modules\Beneficiario\Model\codigo_nomina;

class CodigoNominaController extends Controller{

		protected $titulo = 'Codigo Nomina';

	public $librerias = [
		'alphanum',
		'maskedinput',
		'datatables',
	];

	public $js = ['codigoNomina'];

	public function __construct(){
		parent::__construct();
	}

	public function getIndex() {
		return $this->view('beneficiario::codigoNomina');
	}

	public function getBuscar(Request $request, $id = 0){
		$codigo_nomina = codigo_nomina::find($id);
		
		if ($codigo_nomina){
			return array_merge($codigo_nomina->toArray(), [
				's' => 's', 
				'msj' => trans('controller.buscar')
			]);
		}
		
		return trans('controller.nobuscar');
	}

	public function postCrear(CodigoNominaRequest $request){
		DB::beginTransaction();
		try{
			$codigo_nomina = codigo_nomina::create($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function putActualizar(CodigoNominaRequest $request, $id = 0){
		DB::beginTransaction();
		try{
			$codigo_nomina = codigo_nomina::find($id)->update($request->all());
		}catch(Exception $e){
			DB::rollback();
			return $e->errorInfo[2];
		}
		DB::commit();

		return ['s' => 's', 'msj' => trans('controller.incluir')];
	}

	public function deleteEliminar(Request $request, $id = 0){
		try{
			$codigo_nomina = codigo_nomina::destroy($id);
		}catch(Exception $e){
			return $e->errorInfo[2];
		}

		return ['s' => 's', 'msj' => trans('controller.eliminar')];
	}

	public function getDatatable(){
		$sql = codigo_nomina::select([
			'codigo_nomina.id','codigo_nomina.nombre', 'codigo_nomina.codigo' 
		]);

	    return Datatables::of($sql)->setRowId('id')->make(true);
	}
    
}
