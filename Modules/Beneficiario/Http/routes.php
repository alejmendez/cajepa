<?php

Route::group(['middleware'=>'web', 'prefix' => Config::get('admin.prefix')], function() {

		Route::controllers([
		'municipio'			=> 'MunicipioController',
		'codigo_nomina'			=> 'CodigoNominaController',
		'codigo_condicional'		=> 'CodigoCondicionalController',
		'tipo_nomina'					=>'TipoNominaController',
		'estatus'							=>'EstatusController',
		'tipos_egresos'							=>'TiposEgresosController',
		'registroB'									=>'BeneficiarioController',
		'ingresos'										=>'EstadoCuentaIngresosController',
		'estado_cuenta'									 	=>'EstadoCuentaController',
		'subir'										  	     	=>'SubirController',
		
	]);

		Route::post('excel', ['uses' =>'CargarController@postCrear', 'as' => 'excel']);
		Route::get('cargar', ['uses' =>'CargarController@getIndex', 'as' => 'cargar']);
		Route::get('recibo', ['uses' =>'EstadoCuentaController@getRecibo', 'as' => 'recibo']);
});