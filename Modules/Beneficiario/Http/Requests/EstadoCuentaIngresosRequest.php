<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class EstadoCuentaIngresosRequest extends Request {
	protected $tabla = 'estado_cuenta_ingresos';
	protected $rules = [
		'beneficiario_id' => ['required', 'integer'], 
		'fecha_aporte' => ['required'], 
		'condicional_id' => ['required', 'integer'], 
		'aporte' => ['required', 'integer'], 
		'observacion' => ['required', 'min:3', 'max:255']
	];

	public function rules(){
		return $this->reglas();
	}
}