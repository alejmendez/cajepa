<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;


class TiposEgresosRequest extends Request
{
   protected $tabla = 'tipo_egresos';
   
   protected $rules =  [
    'nombre'=>['required','min:3','max:255']
   ];

    public function rules(){
        return $this->reglas();
    }
}
