<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;


class TipoNominaRequest extends Request
{
   protected $tabla = 'tipo_nomina';
   
   protected $rules =  [
    'nombre'=>['required','min:3','max:255']
   ];

    public function rules(){
        return $this->reglas();
    }
}
