<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class NominaRequest extends Request {
	protected $tabla = 'nomina';
	protected $rules = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}