<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class EstadoRequest extends Request {
	protected $tabla = 'estado';
	protected $rules = [
		'nombre' => ['required', 'min:3', 'max:255']
	];

	public function rules(){
		return $this->reglas();
	}
}