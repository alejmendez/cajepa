<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class BeneficiarioRequest extends Request {
	protected $tabla = 'beneficiario';
	protected $rules = [
		'cedula' => ['required', 'min:3', 'max:255'], 
		'nombres' => ['required', 'min:3', 'max:255'], 
		'apellidos' => ['required', 'min:3', 'max:255'], 
		'municipio_id' => ['required', 'integer'], 
		'nomina_id' => ['required', 'integer'], 
		'codigo_exp' => ['required', 'min:3', 'max:255'], 
		'fecha_ingreso' => ['required'], 
		'tipo_nomina_id' => ['required', 'integer'], 
		'estatus_id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}