<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class ParroquiaRequest extends Request {
	protected $tabla = 'parroquia';
	protected $rules = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'municipio_id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}