<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class MunicipioRequest extends Request {
	protected $tabla = 'municipio';
	protected $rules = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'estado_id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}