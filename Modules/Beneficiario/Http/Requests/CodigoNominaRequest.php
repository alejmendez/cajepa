<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;


class CodigoNominaRequest extends Request
{
   protected $tabla = 'codigo_nomina';
   
   protected $rules =  [
    'nombre'=>['required','min:3','max:255'],
    'codigo'=>['required','min:3','max:255']
   ];

    public function rules(){
        return $this->reglas();
    }
}
