<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;


class EstatusRequest extends Request
{
   protected $tabla = 'estatus';
   
   protected $rules =  [
    'nombre'=>['required','min:3','max:255']
   ];

    public function rules(){
        return $this->reglas();
    }
}
