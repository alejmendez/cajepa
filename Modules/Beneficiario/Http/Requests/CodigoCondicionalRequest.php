<?php

namespace Modules\Beneficiario\Http\Requests;

use App\Http\Requests\Request;
 
class CodigoCondicionalRequest extends Request {
	protected $tabla = 'codigo_condicional';
	protected $rules = [
		'nombre' => ['required', 'min:3', 'max:255'], 
		'codigo_nomina_id' => ['required', 'integer']
	];

	public function rules(){
		return $this->reglas();
	}
}