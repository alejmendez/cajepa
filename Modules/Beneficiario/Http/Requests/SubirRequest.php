<?php 

namespace Modules\Beneficiario\Http\Requests;
use App\Http\Requests\Request;

class SubirRequest extends Request {

	protected $rules = [
	    'fecha' => ['required'],
		'monto' => ['required','min:3', 'max:50'],
		'cedula'=> ['required','min:3'],
		'nombre'=> ['required', 'min:3', 'max:50'],
		'concepto'=> ['required','min:3', 'max:50']
	];

	public function rules() {
		return $this->reglas();
	}
}
