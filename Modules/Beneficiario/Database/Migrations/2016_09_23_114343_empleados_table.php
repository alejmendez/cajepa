<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('estado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            
            
        });
            Schema::create('municipio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('estado_id')->unsigned();
            $table->timestamps();

            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade')->onUpdate('cascade');

           
        });

        Schema::create('parroquia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('municipio_id')->unsigned();
            $table->timestamps();

            $table->foreign('municipio_id')->references('id')->on('municipio')->onDelete('cascade')->onUpdate('cascade');
            
        });


        Schema::create('codigo_nomina', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
             $table->string('codigo');
            $table->timestamps();
            
        });
        
          


        Schema::create('estatus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            
        });

        Schema::create('tipo_nomina', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            
        });




     Schema::create('beneficiario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula')->unique();
            $table->string('nombres');
            $table->string('apellidos');
            $table->integer('municipio_id')->unsigned();
            $table->integer('nomina_id')->unsigned();
            //$table->integer('condicional_id')->unsigned();
            $table->string('codigo_exp');
            $table->timestamp('fecha_ingreso');
            $table->integer('tipo_nomina_id')->unsigned();
            $table->integer('estatus_id')->unsigned();
            $table->text('observacion');
            $table->timestamps();

            $table->foreign('municipio_id')->references('id')->on('municipio')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nomina_id')->references('id')->on('codigo_nomina')->onDelete('cascade')->onUpdate('cascade');
            //$table->foreign('condicional_id')->references('id')->on('codigo_condicional')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('estatus_id')->references('id')->on('estatus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tipo_nomina_id')->references('id')->on('tipo_nomina')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiario');
        Schema::dropIfExists('tipo_nomina');
        Schema::dropIfExists('estatus');
        Schema::dropIfExists('codigo_condicional');
        Schema::dropIfExists('codigo_nomina');
        Schema::dropIfExists('parroquia');
        Schema::dropIfExists('municipio');
        Schema::dropIfExists('estado');
    }
}