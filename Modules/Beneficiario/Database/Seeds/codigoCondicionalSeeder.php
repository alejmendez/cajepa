<?php 

namespace Modules\Beneficiario\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

use Modules\Beneficiario\Model\codigo_condicional;

class codigoCondicionalSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$codigo_condicionales = [
			[1, 'a55'],
			[1, 'a95'],
			[1, 'a74'],
			[2, 'b55'],
			[2, 'b95'],
			[2, 'b72'],
			[3, 'g55'],
			[3, 'g95'],
			[3, 'g72'],
			[4, 'h55'],
			[4, 'h95'],
			[4, 'h74'],
			[5, 'm55'],
			[5, 'm95'],
			[5, 'm74'],
			[6, 'n55'],
			[6, 'n95'],
			[6, 'n74'],
			[7, 'y55'],
			[7, 'y95'],
			[7, 'y74']

		];

		DB::beginTransaction();
		try{
			foreach ($codigo_condicionales as $codigo_condicional) {
				codigo_condicional::create([
					'codigo_nomina_id' => $codigo_condicional[0]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	}
}