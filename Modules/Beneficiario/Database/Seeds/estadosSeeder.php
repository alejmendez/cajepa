<?php 

namespace Modules\Beneficiario\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

use Modules\Beneficiario\Model\estado;

class estadosSeeder extends Seeder {

	public function run()
	{
		$estados = [
			'Amazonas',
			'Anzoátegui',
			'Apure',
			'Aragua',
			'Barinas',
			'Bolívar',
			'Carabobo',
			'Cojedes',
			'Delta Amacuro',
			'Falcón',
			'Guárico',
			'Lara',
			'Mérida',
			'Miranda',
			'Monagas',
			'Nueva Esparta',
			'Portuguesa',
			'Sucre',
			'Táchira',
			'Trujillo',
			'Vargas',
			'Yaracuy',
			'Zulia',
			'Distrito Capital',
			'Dependencias Federales'
		];

		foreach ($estados as $estado) {
			estado::create([
				'nombre' => $estado,
			]);
		}
	}
}