<?php namespace Modules\Beneficiario\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Admin\Model\app_menu as menu;


class MenuSeederTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$def = menu::create([
			'nombre' => 'Definiciones',
			'padre' => 0,
			'posicion' => 1,
			'direccion' => '#Definiciones',
			'icono' => 'fa fa-file-text-o'
		]);

		 menu::create([
			'nombre' => 'Municipio',
			'padre' => $def->id,
			'posicion' => 0,
			'direccion' => 'municipio',
			'icono' => 'fa fa-file-text-o'
		]);
		menu::create([
			'nombre' => 'Codigo Nomina',
			'padre' => $def->id,
			'posicion' => 1,
			'direccion' => 'codigo_nomina',
			'icono' => 'fa fa-file-text-o'
		]);
		menu::create([
			'nombre' => 'Codigo Condicional',
			'padre' => $def->id,
			'posicion' => 2,
			'direccion' => 'codigo_condicional',
			'icono' => 'fa fa-file-text-o'
		]);
		menu::create([
			'nombre' => 'Tipo Nomina',
			'padre' => $def->id,
			'posicion' => 3,
			'direccion' => 'tipo_nomina',
			'icono' => 'fa fa-file-text-o'
		]);
		menu::create([
			'nombre' => 'Estatus',
			'padre' => $def->id,
			'posicion' => 4,
			'direccion' => 'estatus',
			'icono' => 'fa fa-file-text-o'
		]);
		menu::create([
			'nombre' => 'Tipos Egresos',
			'padre' => $def->id,
			'posicion' => 5,
			'direccion' => 'tipos_egresos',
			'icono' => 'fa fa-file-text-o'
		]);

		menu::create([
			'nombre' => 'Registro Beneficiario',
			'padre' => 0,
			'posicion' => 2,
			'direccion' => 'registroB',
			'icono' => 'fa fa-file-text-o'
		]);

	menu::create([
			'nombre' => 'Ingresos',
			'padre' => 0,
			'posicion' => 3,
			'direccion' => 'ingresos',
			'icono' => 'fa fa-file-text-o'
		]);
	menu::create([
			'nombre' => 'Egresos',
			'padre' => 0,
			'posicion' => 4,
			'direccion' => 'egresos',
			'icono' => 'fa fa-file-text-o'
		]);

	menu::create([
			'nombre' => 'Estado de Cuenta',
			'padre' => 0,
			'posicion' => 5,
			'direccion' => 'estado_cuenta',
			'icono' => 'fa fa-file-text-o'
		]);
		
	}

}