<?php

namespace Modules\Beneficiario\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BeneficiarioDatabaseSeeder extends Seeder
{
	
	public function run()
	{
		Model::unguard();

			$this->call(estadosSeeder::class);
			$this->call(municipiosSeeder::class);
			$this->call(parroquiasSeeder::class);
			$this->call(codigoNominaSeeder::class);
			$this->call(codigoCondicionalSeeder::class);
			$this->call(MenuSeederTableSeeder::class);

		Model::reguard();

	}
}
