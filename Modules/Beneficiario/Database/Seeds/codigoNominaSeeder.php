<?php 

namespace Modules\Beneficiario\Database\Seeds;

use DB;
use Illuminate\Database\Seeder;

use Modules\Beneficiario\Model\codigo_nomina;

class codigoNominaSeeder extends Seeder {
	public function run()
	{
		$codigo_nominas = [
			['Nomina', '001'],
			['Nomina', '002'],
			['Nomina', '006'],
			['Nomina', '007'],
			['Nomina', '013'],
			['Nomina', '014'],
			['Nomina', '020']
		];
		DB::beginTransaction();

		try{
			foreach ($codigo_nominas as $codigo_nomina) {
				codigo_nomina::create([
					'nombre' => $codigo_nomina[0],
					'codigo' => $codigo_nomina[1]
				]);
			}
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	}
}