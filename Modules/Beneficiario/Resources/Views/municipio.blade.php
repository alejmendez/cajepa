@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Nombre' => '50',
		'Estado' => '50'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Municipio</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre',
				'placeholder' => 'Nombre',
				'required' => 'required'
			]) }} 
			
			{{ Form::bsSelect('estado_id', $controller->estados(), [
				'label' => 'Estado',
				'placeholder' => 'Estado',
				'required' => 'required'
			]) }}
		
		{!! Form::close() !!}
	</div>
@endsection