@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Cedula' => '9.0909090909091',
		'Nombres' => '9.0909090909091',
		'Apellidos' => '9.0909090909091',
		'Municipio Id' => '9.0909090909091',
		'Nomina Id' => '9.0909090909091',
		'Condicional Id' => '9.0909090909091',
		'Codigo Exp' => '9.0909090909091',
		'Fecha Ingreso' => '9.0909090909091',
		'Tipo Nomina Id' => '9.0909090909091',
		'Estatus Id' => '9.0909090909091',
		'Observacion' => '9.0909090909091'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Beneficiario</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		
			{{ Form::bsText('cedula', '', [
				'label' => 'Cedula',
				'placeholder' => 'Cedula',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('nombres', '', [
				'label' => 'Nombres',
				'placeholder' => 'Nombres',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('apellidos', '', [
				'label' => 'Apellidos',
				'placeholder' => 'Apellidos',
				'required' => 'required'
			]) }} 

			{{ Form::bsSelect('estado_id', $controller->estados(), [
				'label' => 'Estado',
				'placeholder' => 'Estado',
				'required' => 'required'
			]) }}

			{{ Form::bsSelect('municipio_id', [],'', [
				'label' => 'Municipio',
				'placeholder' => 'Municipio',
				'required' => 'required'
			]) }} 

			{{ Form::bsSelect('nomina_id', $controller->nominas(), [
				'label' => 'Nomina ',
				'placeholder' => 'Nomina',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('codigo_exp', '', [
				'label' => 'Codigo Expediente',
				'placeholder' => 'Codigo Expediente',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('fecha_ingreso', '', [
				'label' => 'Fecha Ingreso',
				'placeholder' => 'Fecha Ingreso',
				'required' => 'required'
			]) }} 
			{{ Form::bsSelect('tipo_nomina_id', $controller->tipo_nomina(), [
				'label' => 'Tipo Nomina ',
				'placeholder' => 'Tipo Nomina ',
				'required' => 'required'
			]) }} 
			{{ Form::bsSelect('estatus_id', $controller->estatus(), [
				'label' => 'Estatus Id',
				'placeholder' => 'Estatus Id',
				'required' => 'required'
			]) }} 
	<div class="col-md-12">
		<div class="form-group">
			<textarea class="form-control" name="observacion" id="observacion" placeholder="Observacion"></textarea>
		</div>
	</div>
		{!! Form::close() !!}
	</div>
@endsection