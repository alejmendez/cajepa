<style type="text/css">
#reporteHtml {
	position: relative;
	color: #001028;
	background: #FFFFFF;
	font-size: 5px; 
	font-family: Arial;
}

#reporteHtml .clearfix:after {
	content: "";
	display: table;
	clear: both;
}

#reporteHtml a {
	color: #5D6975;
	text-decoration: underline;
}

#reporteHtml .header {
	padding: 10px 0;
	margin-bottom: 30px;
}

#reporteHtml #logo {
	text-align: center;
	margin-bottom: 10px;
}

#reporteHtml #logo img {
	width: 90px;
}

#reporteHtml h1 {
	border-top: 1px solid  #5D6975;
	border-bottom: 1px solid  #5D6975;
	color: #5D6975;
	font-size: .4em;
	line-height: 1.4em;
	font-weight: normal;
	text-align: center;
}

#reporteHtml #project {
	float: left;
}

#reporteHtml #project span {
	color: #5D6975;
	text-align: right;
	margin-right: 10px;
	display: inline-block;
}

#reporteHtml #company {
	float: right;
	text-align: right;
}

#reporteHtml #project div,
#reporteHtml #company div {
	white-space: nowrap;        
}
#cal{
	margin-left: 50px;
}
#reporteHtml table {
	width: 100%;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 20px;
	font-family: Arial;
}

#reporteHtml table tr:nth-child(2n-1) td {
	background: #F5F5F5;
}

#reporteHtml table th,
#reporteHtml table td {
	text-align: center;
}

#reporteHtml table th {
	padding: 5px 20px;
	color: #5D6975;
	border-bottom: 1px solid #C1CED9;
	white-space: nowrap;        
	font-weight: normal;
}

#reporteHtml table .service,
#reporteHtml table .desc {
	text-align: left;
}

#reporteHtml table td {
	padding: 10px 20px;
	text-align: right;
}
#reporteHtml table td.service,
#reporteHtml table td.desc {
	vertical-align: top;
}

#reporteHtml table td.unit,
#reporteHtml table td.qty,
#reporteHtml table td.total {
	font-size: 1.2em;
}

#reporteHtml table td.grand {
	border-top: 1px solid #5D6975;;
}

#reporteHtml .footer {
	color: #5D6975;
	width: 100%;
	height: 30px;
	position: absolute;
	bottom: 0;
	border-top: 1px solid #C1CED9;
	padding: 8px 0;
	text-align: center;
}
#nom{
	margin-left: 522px;
}

table {
	
	border: 1px solid #777777;border-spacing: 0;border-collapse: collapse;font-size: 12px;width: 800px;
}

table td{

	border: 1px solid #777777;
}

table td { 
	padding: 3px;
}
	
table .titulo { 
	font-weight: bold;
	background: #f4f4f4;
	margin: 25px 0 0 160px !important;
}
}

table .par {
	background: #efefff;
}

table .centrado { 
	    
	text-align: center;
}
#titulo_recibo{
	float: left;
	margin: 25px 0 0 160px;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
}
</style>

<div id="reporteHtml">

<div> 

   <table style="">			
	<tr>
		<td colspan="8">
		<div >
			<img src="{{asset('public/img/logos/logo.png')}}"  style="float: left;width: 120px;">
		</div>	
		</td>
	</tr>
	<tr>
		<td class="titulo" colspan="8"><center><h4 style="margin-left: 100px"><center>CAJA DE AHORRO DE LOS EMPLEADOS, JUBILADOS y PENSIONADOS ADMINISTRATIVOS DE LA
					GOBERNACIÓN DEL ESTADO BOLIVAR. (CAEJPA-GEB)
					Registro Nº 766 del Sector Público Rif. J-31336163-1
					</center></h4></center></td>
	</tr>
	

	<tr>
		<td class="titulo centrado">CEDULA</td>
		<td class="titulo centrado" colspan="4">NOMBRE Y APELLIDO</td>
		<td class="titulo centrado" colspan="3">FECHA DE INGRESO</td>
	</tr>
	<tr>
		<td>{{$empleado->codemp }}</td>
		<td colspan="4">{{$empleado->nomemp }}</td>
		<td colspan="3">{{$empleado->fecnom}}</td>
	</tr>
	<tr><td colspan="8">&nbsp;</td></tr>

	<tr class="titulo">
		<td class="centrado" colspan="4">Conceptos</td>
		<td class="centrado" colspan="4">Monto</td>
	</tr>
			<tr>
					<td class="centrado" colspan="4">Retenciones</td>
					<td class="centrado" colspan="4">{{$aporte}}</td>
			</tr>

			<tr>
					<td class="centrado" colspan="4">Aporte Patronal</td>
					<td class="centrado" colspan="4">{{$aporte}}</td>
			</tr>
		<!-- 	<tr>
					<td class="centrado" colspan="4">Deuda Prestamo</td>
					<td class="centrado" colspan="4">&nbsp;</td>
			</tr> -->
			<tr><td colspan="8">&nbsp;</td></tr>
			<tr>
					<td class="centrado" colspan="4">Total Ahorros</td>
					<td class="centrado" colspan="4">{{$totalahorros}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Retiro Parcial 80%</td>
					<td class="centrado" colspan="4">{{$por_80}} </td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Fondo de Reserva</td>
					<td class="centrado" colspan="4">{{$por_20}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Retiros</td>
					<td class="centrado" colspan="4">{{$retiros}}</td><!-- suma de todos los retiros -->
			</tr>
			<tr>
					<td class="centrado" colspan="4">Prestamos</td>
					<td class="centrado" colspan="4">{{$prestamos}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Gastos Administrativos</td>
					<td class="centrado" colspan="4">{{$gasto}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Diferencia</td>
					<td class="centrado" colspan="4">{{$retiroparcial}}</td><!-- retiro parcial -->
			</tr>
			<tr>
					<td class="centrado" colspan="4">Retiro Parcial al 80%</td>
					<td class="centrado" colspan="4">{{$parcial_80}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Fondo Reserva</td>
					<td class="centrado" colspan="4">{{$parcial_20}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Interes Retenido al 12% del Prestamo</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<!-- <tr>
					<td class="centrado" colspan="4">Monto Total del prestamo</td>
					<td class="centrado" colspan="4"></td>
			</tr> -->





	<!--<tr><td colspan="8">&nbsp;</td></tr>-->
	<tr><td colspan="8">&nbsp;</td></tr>
	<tr>
		<td class="titulo" style="text-align: right;" colspan="4">
			<div style="margin-right: 50px;">NETO A COBRAR BS:</div>
			<div style="width: 100px;"></div>
		</td>
		<td colspan="4">{{ $neto }}</td>
	</tr>
</table>

	
</div>
	
</div>