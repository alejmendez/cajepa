<style type="text/css">
#reporteHtml {
	position: relative;
	color: #001028;
	background: #FFFFFF;
	font-size: 12px; 
	font-family: Arial;
}

#reporteHtml .clearfix:after {
	content: "";
	display: table;
	clear: both;
}

#reporteHtml a {
	color: #5D6975;
	text-decoration: underline;
}

#reporteHtml .header {
	padding: 10px 0;
	margin-bottom: 30px;
}

#reporteHtml #logo {
	text-align: center;
	margin-bottom: 10px;
}

#reporteHtml #logo img {
	width: 90px;
}

#reporteHtml h1 {
	border-top: 1px solid  #5D6975;
	border-bottom: 1px solid  #5D6975;
	color: #5D6975;
	font-size: 2.4em;
	line-height: 1.4em;
	font-weight: normal;
	text-align: center;
	margin: 0 0 20px 0;
}

#reporteHtml #project {
	float: left;
}

#reporteHtml #project span {
	color: #5D6975;
	text-align: right;
	margin-right: 10px;
	display: inline-block;
}

#reporteHtml #company {
	float: right;
	text-align: right;
}

#reporteHtml #project div,
#reporteHtml #company div {
	white-space: nowrap;        
}
#cal{
	margin-left: 50px;
}
#reporteHtml table {
	width: 100%;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 20px;
	font-family: Arial;
}

#reporteHtml table tr:nth-child(2n-1) td {
	background: #F5F5F5;
}

#reporteHtml table th,
#reporteHtml table td {
	text-align: center;
}

#reporteHtml table th {
	padding: 5px 20px;
	color: #5D6975;
	border-bottom: 1px solid #C1CED9;
	white-space: nowrap;        
	font-weight: normal;
}

#reporteHtml table .service,
#reporteHtml table .desc {
	text-align: left;
}

#reporteHtml table td {
	padding: 10px 20px;
	text-align: right;
}
#reporteHtml table td.service,
#reporteHtml table td.desc {
	vertical-align: top;
}

#reporteHtml table td.unit,
#reporteHtml table td.qty,
#reporteHtml table td.total {
	font-size: 1.2em;
}

#reporteHtml table td.grand {
	border-top: 1px solid #5D6975;;
}

#reporteHtml .footer {
	color: #5D6975;
	width: 100%;
	height: 30px;
	position: absolute;
	bottom: 0;
	border-top: 1px solid #C1CED9;
	padding: 8px 0;
	text-align: center;
}
#nom{
	margin-left: 522px;
}
</style>

<div id="reporteHtml">
	<div>
		<table border="1">
		    <tr>
				<td colspan="8">
				<div >
					<img src="{{asset('public/img/logos/logo.png')}}"  style="float: left;width: 120px;">
				</div>	
				</td>
			</tr>
			<tr>
				<td class="titulo" colspan="8"><center><h4 style="margin-left: 100px"><center>CAJA DE AHORRO DE LOS EMPLEADOS, JUBILADOS y PENSIONADOS ADMINISTRATIVOS DE LA
							GOBERNACIÓN DEL ESTADO BOLIVAR. (CAEJPA-GEB)
							Registro Nº 766 del Sector Público Rif. J-31336163-1 quien coño eresssss?
							</center></h4></center></td>
			</tr>
			<tr>
			    <td align="left" colspan="2">Cedula</td>
			    <td  colspan="8" align="center">{{$empleado->codemp }}<br></td>
	    	</tr>
	    	<tr>
			    <td align="left" colspan="2">Nombre y Apellido</td>
			    <td  colspan="8" align="center">{{$empleado->nomemp }}</td>
	    	</tr>
	    	<tr>
			    <td align="left" colspan="2">Fecha de Ingreso</td>
			    <td  colspan="8" align="center">{{$empleado->fecnom}}</td>
	    	</tr>
	   	 	
		</table>
		<table border="1" >

		   	<tr class="titulo">
				<td  colspan="1" align="center" style="width: 50%;"> <strong> Conceptos</strong></td>
				<td  colspan="9"  align="center"><strong>Monto</strong></td>
			</tr>
			<tr>
			    <td align="left" colspan="1">Retenciones</td>
			    <td  colspan="9" align="center">{{$aporte}} Bs</td>
	    	</tr>
			<tr>
			    <td align="left" colspan="1">Aporte Patronales</td>
			    <td colspan="9" align="center">{{$aporte}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Total Ahorros</td><!-- total de ahorros desde que entro a caja de ahorro -->
		      <td colspan="9" align="center">{{$totalahorros}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Retiro Parcial 80%</td>
		      <td colspan="9"  align="center">{{$por_80}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Fondo Reserva</td>
		      <td colspan="9" align="center">{{$por_20}} Bs</td>
	    	</tr>
			
		</table>

		<table border="1"  >
			<tr>
			    <td align="left" colspan="1" style="width: 50%;" >Gastos Administrativos</td> <!-- total de ahorros menos los retiros menos el gasto administrativo menos el servicio de transferencia si pasa los 30 -->
			    <td colspan="9"  align="center" >{{ $gasto }} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1" >Retiros Parciales </td>
		      <td colspan="9" align="center">{{ $retiros}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Prestamos</td>
		      <td colspan="9" align="center">{{ $prestamos }} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Total Retiros</td>
		      <td colspan="9" align="center"> {{ $retiroparcial}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">80%</td>
		      <td colspan="9" align="center">{{ $parcial_80}} Bs</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Fondo de Reserva 20%</td>
		      <td colspan="9" align="center">{{$parcial_20}} Bs</td>
	    	</tr>
	    	<!-- <tr>
		      <td align="left" colspan="1">Intereses Retenidos al 12% de Prestamos</td>
		      <td colspan="9">&nbsp;</td>
	    	</tr>
	    	<tr>
		      <td align="left" colspan="1">Monto Total del Prestamo</td>
		      <td colspan="9">&nbsp;</td>
	    	</tr> -->
		</table>		
	</div>
</div>