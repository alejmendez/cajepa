<style type="text/css">
#reporteHtml {
	position: relative;
	color: #001028;
	background: #FFFFFF;
	font-size: 12px; 
	font-family: Arial;
}

#reporteHtml .clearfix:after {
	content: "";
	display: table;
	clear: both;
}

#reporteHtml a {
	color: #5D6975;
	text-decoration: underline;
}

#reporteHtml .header {
	padding: 10px 0;
	margin-bottom: 30px;
}

#reporteHtml #logo {
	text-align: center;
	margin-bottom: 10px;
}

#reporteHtml #logo img {
	width: 90px;
}

#reporteHtml h1 {
	border-top: 1px solid  #5D6975;
	border-bottom: 1px solid  #5D6975;
	color: #5D6975;
	font-size: 2.4em;
	line-height: 1.4em;
	font-weight: normal;
	text-align: center;
	margin: 0 0 20px 0;
}

#reporteHtml #project {
	float: left;
}

#reporteHtml #project span {
	color: #5D6975;
	text-align: right;
	margin-right: 10px;
	display: inline-block;
}

#reporteHtml #company {
	float: right;
	text-align: right;
}

#cal{
	margin-left: 50px;
}
#reporteHtml table {
	width: 100%;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 20px;
	font-family: Arial;
}

#reporteHtml table tr:nth-child(2n-1) td {
	background: #F5F5F5;
}

#reporteHtml table th {
	padding: 5px 20px;
	color: #5D6975;
	border-bottom: 1px solid #C1CED9;
	white-space: nowrap;        
	font-weight: normal;
}

#reporteHtml table .service,
#reporteHtml table .desc {
	text-align: left;
}

#reporteHtml .footer {
	color: #5D6975;
	width: 100%;
	height: 30px;
	position: absolute;
	bottom: 0;
	border-top: 1px solid #C1CED9;
	padding: 8px 0;
	text-align: center;
}
#nom{
	margin-left: 522px;
}
.bf{

	padding: 10px;
	text-align: left;

	
}
.centro{
	text-align: center;
}
.w{

	width: 200px;
	height: 200px;
}
 .b{
 	border-style: solid;
 	border-left-width: 1px;
 	border-right-width: 1px;

 }

</style>

<div id="reporteHtml">
	<div>
		    <table border="1" width="100%" > <!-- Lo cambiaremos por CSS -->

	            <tr> 
	                <td colspan="3">
	               	</td>
	            	<td colspan="2">
	                	
	               	</td>
	               	 <td colspan="2" class="bf">
	               	 	{{ $neto }}
	               	</td>

	            </tr>
	          	 
	          	<tr>
		            <td colspan="7" class ="w" >
		            	 {{$nombre }}<br>
		                {{$monto_letras}}
		              
		            </td>
	          	</tr>
	          	<tr>
		            <td colspan="7" class ="bf">
		            	Ciudad Bolivar, {{$fecha}}
		            </td>
	          	</tr>		
	          	<tr>
		          	<td class="centro" colspan="7">
			          	NO ENDOSABLE<br>
						caduca a los 30 dias
					</td>
	          	</tr>
	          	<tr>
	          		<td colspan="7">
	          			banco: {{ $banco}}
	          		</td>
	          	</tr>
	          	<tr>
	          		<td colspan="3">CTA.CTE.N°:</td>
	          		<td colspan="4">{{$cuenta }} </td>
	          	</tr>
	          	<tr>
	          		<td colspan="7">CHEQUE.N°: {{$cheque}}</td>
	          	</tr>
	          	<tr>
	          		<td colspan="7">CONCEPTO: Retiro parcial o Prestamo (80%) de Caja de Ahorros de Empleados, Jubilados y Pensionados Administrativos de la Gobernacion del Estado Bolivar, correspondiente al lapso comprendido entre 01/07/2005 al 30/06/2017.</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="4" style="border-style: none" >total de ahorros a la fecha</td>
	          		<td colspan="3" style="border-style: none" >{{ $totalahorros}}</td>
	          	</tr >
	          	<tr class="b">
	          		<td colspan="4" style="border-style: none"> Retiro Parcial o Prestamos </td>
	          		<td colspan="3" style="border-style: none">{{$prestamos}}</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="4" style="border-style: none">Interes</td>
	          		<td colspan="3" style="border-style: none">{{$interes}}</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="4" style="border-style: none">Retiro Parcial o Prestamos del 80% </td>
	          		<td colspan="3" style="border-style: none">{{ $neto_80 }}</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="4" style="border-style: none">Ahorro Restante</td>
	          		<td colspan="3" style="border-style: none">{{ $restante }}</td>	
	          	</tr>
	          	<tr>
	          		<td colspan="2" >RECIBO CONFORME:<br>
	          										<br> 
	          										<br>
	          										<br>
	          										<br>                
	          		</td>
	          		<td colspan="2" ></td>
	          		<td colspan="3" >NOMBRE</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="2" style="border-style: none;">SELLO Y FIRMA</td>
	          		<td colspan="3" style="border-style: none;"></td>
	          		<td colspan="2" style="border-style: none;" >C.I</td>
	          	</tr>
	          	<tr class="b">
	          		<td colspan="7" style="border-style: none;">FECHA</td>
	          	</tr>
	          	<tr >
	          		<td colspan="1">ELABORADO</td>
	          		<td colspan="1">REVISADO</td>
	          		<td colspan="1">CONTABILIZADO</td>
	          		<td colspan="2">AUDITORIA</td>
	          		<td colspan="2">COMPROBANTE</td>
	          	</tr>
	          	<tr>
	          		<td colspan="1">...</td>
	          		<td colspan="1">...</td>
	          		<td colspan="1" >...</td>
	          		<td colspan="1">...</td>
	          		<td colspan="2">...</td>
	          		<td colspan="2">...</td>
	          	</tr>
	        
	        
	          	
        	</table>
	</div>
</div>