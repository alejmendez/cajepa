@extends('admin::layouts.default')
@section('content')
  @include('admin::partials.botonera')

 {{ Form::bsModalBusqueda([
    'fecha' => '20',
    'Monto' => '20',
    'cedula' => '20',
    'nombre' => '20',
    'concepto' => '20', 
  ]) }}
    <ul class="page-breadcrumb breadcrumb">
    <li>
      <a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Subir</span>
    </li>
  </ul>
 
    <div class="row">
      <form id="formulario" name="formulario" enctype="multipart/form-data" role="form" autocomplete="off">

         {{ Form::bsText('fecha', '', [
            'label'     => 'Fecha',
            'placeholder'   => '',
            'required'    => 'required',
            'class_cont'  => 'col-sm-6'
          ]) }}


          {{ Form::bsText('monto', '', [
            'label'     => 'Monto',
            'placeholder'   => '',
            'required'    => 'required',
            'class_cont'  => 'col-sm-6'
          ]) }}

          {{ Form::bsText('cedula', '', [
            'label'     => 'cedula',
            'placeholder'   => 'cedula',
            'required'    => 'required',
            'class_cont'  => 'col-sm-6'
          ]) }}

          {{ Form::bsText('nombre', '', [
            'label'     => 'nombre',
            'placeholder'   => 'nombre',
            'required'    => 'required',
            'class_cont'  => 'col-sm-6'
          ]) }}

          {{ Form::bsText('concepto', '', [
            'label'     => 'concepto',
            'placeholder'   => 'concepto',
            'required'    => 'required',
            'class_cont'  => 'col-sm-6'
          ]) }}

    </form>  
  </div>
@endsection

