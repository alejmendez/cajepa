@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Beneficiario Id' => '20',
		'Fecha Aporte' => '20',
		'Condicional Id' => '20',
		'Aporte' => '20',
		'Observacion' => '20'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Ingresos</span>
		</li>
	</ul>
	
	<div class="row">
	
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
			<ul class="page-breadcrumb breadcrumb">
			<li>
			<h1>Datos Beneficiario</h1>
			</li>
				
			</ul>
		{{ Form::bsText('nombres', '', [
				'label' => 'Nombres',
				'placeholder' => 'Nombres',
				'required' => 'required'
			]) }} 
			{{ Form::bsText('apellidos', '', [
				'label' => 'Apellidos',
				'placeholder' => 'Apellidos',
				'required' => 'required'
			]) }} 
			{{ Form::bsNumber('cedula', '', [
				'label' => 'Cedula',
				'placeholder' => 'Cedula',
				'required' => 'required'
			]) }} 
		
		<div class="row">
			</div>
			<ul class="page-breadcrumb breadcrumb">
			<li>
			<h1>Aporte Patronal</h1>
			</li>
				
			</ul>
			{{ Form::bsNumber('beneficiario_id', '', [
				'label' => 'Beneficiario Id',
				'placeholder' => 'Beneficiario Id',
				'required' => 'required'
			]) }} 
			{{ Form::bsSelect('condicional_id',[], [
				'label' => 'Codigo Condicional',
				'placeholder' => 'Condicional Id',
				'required' => 'required'
			]) }}
			{{ Form::bsText('fecha_aporte', '', [
				'label' => 'Fecha Aporte',
				'placeholder' => 'Fecha Aporte',
				'required' => 'required'
			]) }} 
			{{ Form::bsNumber('aporte', '', [
				'label' => 'Aporte',
				'placeholder' => 'Aporte',
				'required' => 'required'
			]) }} 
		
			<div class="col-md-12">
		<div class="form-group">
			<textarea class="form-control" name="observacion" id="observacion" placeholder="Observacion"></textarea>
		</div>
	</div>
			<div class="row">
			</div>
			<ul class="page-breadcrumb breadcrumb">
			<li>
			<h1>Aporte Socio</h1>
			</li>
				
			</ul>
				 
			{{ Form::bsText('fecha_aporte', '', [
				'label' => 'Fecha Aporte',
				'placeholder' => 'Fecha Aporte',
				'required' => 'required'
			]) }} 
			{{ Form::bsSelect('condicional_id',[], [
				'label' => 'Condicional Id',
				'placeholder' => 'Condicional Id',
				'required' => 'required'
			]) }} 
			{{ Form::bsNumber('aporte', '', [
				'label' => 'Aporte',
				'placeholder' => 'Aporte',
				'required' => 'required'
			]) }} 
		
			<div class="col-md-12">
		<div class="form-group">
			<textarea class="form-control" name="observacion" id="observacion" placeholder="Observacion"></textarea>
		</div>
	</div>
	<div class="row">
			</div>
			<ul class="page-breadcrumb breadcrumb">
			<li>
			<h1>Prestamos</h1>
			</li>
				
			</ul>
			 
			{{ Form::bsText('fecha_aporte', '', [
				'label' => 'Fecha Aporte',
				'placeholder' => 'Fecha Aporte',
				'required' => 'required'
			]) }} 
			{{ Form::bsSelect('condicional_id', [], [
				'label' => 'Condicional Id',
				'placeholder' => 'Condicional Id',
				'required' => 'required'
			]) }} 
			{{ Form::bsNumber('aporte', '', [
				'label' => 'Aporte',
				'placeholder' => 'Aporte',
				'required' => 'required'
			]) }} 
		
			<div class="col-md-12">
		<div class="form-group">
			<textarea class="form-control" name="observacion" id="observacion" placeholder="Observacion"></textarea>
		</div>
	</div>
		
		{!! Form::close() !!}
	</div>
@endsection