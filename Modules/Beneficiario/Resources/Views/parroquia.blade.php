@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Nombre' => '50',
		'Municipio Id' => '50'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Parroquia</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
		
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre',
				'placeholder' => 'Nombre',
				'required' => 'required'
			]) }} 
			{{ Form::bsNumber('municipio_id', '', [
				'label' => 'Municipio Id',
				'placeholder' => 'Municipio Id',
				'required' => 'required'
			]) }}
		
		{!! Form::close() !!}
	</div>
@endsection