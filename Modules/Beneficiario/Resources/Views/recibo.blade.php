
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Recibo de Pago</title>
</head>
<body>
<style type="text/css">
	
	table {
		border: 1px solid #777777;border-spacing: 0;border-collapse: collapse;font-size: 12px;width: 800px;
	}
	table td{
		border: 1px solid #777777;
	}
	table td { 
	    padding: 3px;
	}
	table .titulo { 
	    font-weight: bold;
	    background: #f4f4f4;
	}
	table .par {
		background: #efefff;
	}
	table .centrado { 
	    text-align: center;
	}
	#titulo_recibo{
	float: left;
	margin: 25px 0 0 160px;
	font-size: 14px;
	font-weight: bold;
	text-align: center;
}
</style>
<table style="">			
	<tr>
		<td colspan="8">
			<img src="{{asset('public/img/logos/logo.png')}}"  style="float: left;width: 120px;">
				<h2><center>CAJA DE AHORRO DE LOS EMPLEADOS, JUBILADOS y PENSIONADOS ADMINISTRATIVOS DE LA
				GOBERNACIÓN DEL ESTADO BOLIVAR. (CAEJPA-GEB)
				Registro Nº 766 del Sector Público Rif. J-31336163-1
				</center></h2>
			<div id="titulo_recibo">
				
				
			</div>
		</td>
	</tr>

	<tr>
		<td class="titulo" colspan="8"><center>N&Oacute;MINA:</center></td>
	</tr>
	

	<tr>
		<td class="titulo centrado">CEDULA</td>
		<td class="titulo centrado" colspan="4">NOMBRE Y APELLIDO</td>
		<td class="titulo centrado" colspan="3">FECHA DE INGRESO</td>
	</tr>
	<tr>
		<td>{{$empleado->codemp}}</td>
		<td colspan="4">{{$empleado->nomemp}}</td>
		<td colspan="3"></td>
	</tr>
			

	<tr><td colspan="8">&nbsp;</td></tr>

	<tr class="titulo">
		<td class="centrado" colspan="4">Conceptos</td>
		<td class="centrado" colspan="4">Monto</td>
	</tr>
			<tr>
					<td class="centrado" colspan="4">Ahorro Socio</td>
					<td class="centrado" colspan="4">{{$ahorro}}</td>
			</tr>

			<tr>
					<td class="centrado" colspan="4">Aporte Patronal</td>
					<td class="centrado" colspan="4">{{$ahorro}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Deuda Prestamo</td>
					<td class="centrado" colspan="4">&nbsp;</td>
			</tr>
			<tr><td colspan="8">&nbsp;</td></tr>
			<tr>
					<td class="centrado" colspan="4">TOTAL AHORROS ACUMULADOS</td>
					<td class="centrado" colspan="4">{{$resta}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">80% AHORROS DISPONIBLES</td>
					<td class="centrado" colspan="4">{{$resultadototal}}</td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Retenciones</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Fondo de Reserva</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Retiro Parciales</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">total de retiros</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Diferencia</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Fondo Reserva</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Interes Retenido al 12% del Prestamo</td>
					<td class="centrado" colspan="4"></td>
			</tr>
			<tr>
					<td class="centrado" colspan="4">Monto Total del prestamo</td>
					<td class="centrado" colspan="4"></td>
			</tr>





	<!--<tr><td colspan="8">&nbsp;</td></tr>-->
	<tr>
		<td class="titulo" style="text-align: right;" colspan="4"><div style="margin-right: 50px;">Gasto administrativo</div></td>
		
		<td colspan="3"></td>
	</tr>
	<tr><td colspan="8">&nbsp;</td></tr>
	<tr>
		<td class="titulo" style="text-align: right;" colspan="4">
			<div style="margin-right: 50px;">NETO A COBRAR BS:</div>
			<div style="width: 100px;"></div>
		</td>
		<td colspan="4"></td>
	</tr>
</table>

	<script src="jquery-1.12.3.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			console.log('holaaa');
				
		</script>


</body>
</html>