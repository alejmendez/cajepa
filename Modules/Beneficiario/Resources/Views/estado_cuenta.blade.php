@extends('admin::layouts.default')
@section('content')
<div class="container-fluid">
      {{ csrf_field() }}
      <div class="row">
         <div class="col-md-4">
            <!-- Nav tabs -->
            <input class="form-control cedula" type="text"  name="cedu" id="formGroupInputSmall" placeholder="Tu cedula..">
            
           
                  <a class="btn btn-primary" href="#" role="button" id="boton">Buscar</a>
                  <h4 class="text-primary"><strong>Panel de Control</strong></h4>
                  <ul class="nav nav-pills nav-stacked" role="tablist">
                     <li role="presentation" class="active"><a href="#homeDatos" aria-controls="homeDatos" role="tab" data-toggle="tab">Estado de cuenta</a></li>
                     <li role="presentation"><a href="#tablaDatos" aria-controls="tablaDatos" role="tab" data-toggle="tab">Historial de cuenta</a></li>
                     <!--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Cambiar de clave</a></li>-->
                  </ul>
                 <!--  <a ><button type="button" id="caejpa" class="btn btn-success btn-lg active">Recibo CAEJPA</button></a>

               <button type="button" class="btn btn-info btn-lg active">Solicitud de Prestamos</button><br> -->
               <!--  <button id="caejpa" class="btn green-jungle btn-block" style="margin-top: 10px; width: 100%;" >Reporte CAEJPA</button>
                <button id="consulta" class="btn blue btn-block">Consulta CAEJPA</button> -->
                  <!--<a href="{{url('/recibo')}}"><button type="button" class="btn btn-danger btn-lg">Recibo Caja de A. CAEJPA</button></a>-->
            <form id="formReporte" name="formReporte" method="GET"  target="_blank" action="{{ url('estado_cuenta/pdf') }}">
                <button id="caejpa" class="btn blue btn-block">Consulta CAEJPA</button>
                <input type="hidden" name="cedu_consulta" id="cedu_consulta">
                <input type="hidden" name="gasto_consulta" id="gasto_consulta">
                 <input type="hidden" name="extra_consulta" id="extra_consulta">
            </form>
            
            <button id="reporte" class="btn green-jungle btn-block" style="margin-top: 10px; width: 100%;" >Generar Recibo CAEJPA</button>
            
          <form id="formReporte3" name="formReporte3" method="GET" action="{{ url('estado_cuenta/reporte') }}" target="_blank">
              <!--  <button id="prueba" class="btn green-jungle btn-block" style="margin-top: 10px; width: 100%;" >Prueba</button> -->
              
               <div class="modal fade draggable-modal" id="myModal" tabindex="-1" role="basic" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" id="cerrar_modal" data-dismiss="modal" aria-hidden="true"></button>
                           <h4 class="modal-title"></h4>
                        </div>
                           <div class="modal-body"> 
                              <input class="form-control" id="data_beneficiario_id" type="hidden"> 
                              <div class="row">

                                {{ Form::bsSelect('tipo',[
                                          'parcial'=> 'Retiro Parcial',
                                          'prestamo'=>'Prestamo',
                                          'total'=>'Retiro Total'
                  
                                    ],'',[
                                          'class_cont' => 'col-md-12',
                                          'label' => 'Tipo de Recibo',
                                          'required' => 'required'
                                ]) }}

                                <div id="ret-par" style="display: none;">

                                    {{ Form::bsText('fecha_modal', '', [
                                       'label' => 'Fecha',
                                       'placeholder' => 'Fecha',
                                       'class_cont'  => 'col-sm-12'
                                    ]) }}

                                    {{  Form::bsSelect('banco_id',$controller->banco(), '', [
                                       'label'     => 'Banco',
                                       'class_cont'  => 'col-sm-12'
                                    ]) }} 

                                    {{  Form::bsSelect('cuenta_id',[], '', [
                                       'label'     => 'Cuenta',
                                       'class_cont'  => 'col-sm-12'
                                    ]) }} 

                                    {{ Form::bsText('cheque', '', [
                                       'label' => 'Cheque',
                                       'placeholder' => 'cheque',
                                       'class_cont'  => 'col-sm-12'
                                    ]) }}
                                </div>

                                <div id="ret-prest" style="display: none;">
                                    {{ Form::bsText('cuotas', '', [
                                       'label' => 'Cuotas',
                                       'placeholder' => 'Cuotas a Pagar',
                                       'class_cont'  => 'col-sm-12'
                                   ]) }}

                                   {{ Form::bsSelect('fia',[
                                          'si'=> 'si',
                                          'no'=>'no',
                  
                                    ],'',[
                                          'class_cont' => 'col-md-12',
                                          'label' => '¿tiene fiador?'
                                    ]) }}
                                </div> 
                                <div id="fadores" style="display: none;">
                                   {{ Form::bsText('fiador_1', '', [
                                       'label' => 'Fiador',
                                       'placeholder' => 'Fiador',
                                       'class_cont'  => 'col-sm-12'
                                   ]) }} 

                                   {{ Form::bsText('fiador_2', '', [
                                       'label' => 'Fiador',
                                       'placeholder' => 'Fiador',
                                       'class_cont'  => 'col-sm-12'
                                   ]) }} 

                                   {{ Form::bsText('fiador_3', '', [
                                       'label' => 'Fiador',
                                       'placeholder' => 'Fiador',
                                       'class_cont'  => 'col-sm-12'
                                   ]) }} 

                                    {{ Form::bsText('fiador_4', '', [
                                       'label' => 'Fiador',
                                       'placeholder' => 'Fiador',
                                       'class_cont'  => 'col-sm-12'
                                   ]) }}    
                               </div>

                              </div>


                           </div>
                           <div class="modal-footer">
                              <button class="btn green-jungle" id="enviar" type="button" >Enviar</button> 
                              <button class="btn green-jungle" id="limpiar" type="button">Limpiar</button> 
                              <input type="hidden" name="cedu_reporte" id="cedu_reporte">
                              <input type="hidden" name="gasto_reporte" id="gasto_reporte">
                              <input type="hidden" name="extra_reporte" id="extra_reporte">
                              <input type="hidden" name="fia_reporte" id="fia_reporte">
                              <input type="hidden" name="tipo_reporte" id="tipo_reporte">
                           </div>
                     </div>
                                                    <!-- /.modal-content -->
                  </div>
                                                <!-- /.modal-dialog -->
               </div>
          </form>
         </div>
         <div class="col-md-8">
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="homeDatos">
                  <table class="table table-hover">
                     <h4 class="text-primary"><strong>Datos del Beneficiario</strong></h4>
                     <tr>
                        <td class="active">Nombre y Apellido</td>
                        <td class="success" id="nombre"><strong></strong></td>
                     </tr>
                     <tr>
                        <td class="active">Cedula</td>
                        <td class="success" id="cedu"></td>
                     </tr>
                     <hr>
                     <tr>
                        <td class="active">Nro de Nomina</td>
                        <td class="success" id="nom"></td>
                     </tr>
                     <tr>
                        <td class="active">Fecha de Ingreso</td>
                        <td class="success" id="fechaIn"></td>
                     </tr>

                     <tr>
                        <td class="active">Estatus</td>
                        <td class="success" id="estatus"></td>
                     </tr>
                  </table>
                  <h4 class="text-primary"><strong>Aportes y 80%</strong></h4>
                  <table class="table table-hover table-responsive">
                     <tr>
                        <td class="active">Ahorro Socio</td>
                        <td class="success" id="div"></td>
                     </tr>
                     <tr>
                        <td class="active">Aporte Patronal</td>
                        <td class="success" id="divi"></td>
                     </tr>
                     <!-- <tr>
                        <td class="active">Ahorro Voluntario</td>
                        <td class="success">0.0</td>
                     </tr> -->
                     <tr>
                        <td class="active">TOTAL AHORROS ACUMULADOS</td>
                        <td class="success" id="suma"></td>
                     </tr>
                     <tr>
                        <td class="active">80% AHORROS DISPONIBLES</td>
                        <td class="success" id="total"><strong></strong></td>
                     </tr>
                     <tr>
                        <td class="active">20% FONDO DE RESERVA</td>
                        <td class="success" id="reserva"><strong></strong></td>
                     </tr>
                     <tr>
                        <td class="active">GASTOS ADMINISTRATIVOS</td>
                        <td class="success" id="gast">
                           <input class="form-control cedula" type="text"  name="gasto" id="gasto" placeholder="...">
                        </td>
                     </tr>
                     <tr>
                        <td class="active">GASTOS EXTRAS</td>
                        <td class="success" id="extra">
                           <input class="form-control cedula" type="text"  name="extras" id="extras" placeholder="...">
                        </td>
                     </tr> 
                  </table>
                  <h4 class="text-primary"><strong>Prestamos</strong></h4>
                  <table class="table table-bordered table-responsive">
                     <tr>
                        <td class="danger">Tipo de Prestamos</td>
                        <!-- <td class="danger">Monto</td> -->
                        <td class="danger">Fecha cuotas</td>
                        <td class="danger">Ult. prestamos</td>
                        <td class="danger">Cuotas</td>
                        <td class="danger">Saldo</td>
                     </tr>
                     <tr>
                        <td class="">CUOTA ESPECIALES PREST. FIANZA</td>
                        <!-- <td id="monto"></td> -->
                        <td class="" id="fechispre"></td>
                        <td class="" id="presmonto"></td>
                         <td class="success" id="monpre"></td>
                        <td class="success" id="saldo"></td>
                     </tr>      
                    <tr>
                        <td class="success" id="totalpres"></td>
                     </tr>

                  </table>
                  <h4 class="text-primary"><strong>Retiros</strong></h4>
                  <table class="table table-hover table-responsive">
                     <tr>
                        <td class="active">Fecha del ultimo Retiro</td>
                        <td class="success" id="retfecha"></td>
                     </tr>
                     <tr>
                        <td class="active" id="retconcepto"></td>
                        <td class="success" id="retmonto"></td>
                     </tr>
                     <!--  <tr>
                        <td class="active" id="">TOTAL RETIROS</td>
                        <td class="success" id="totalret"></td>
                     </tr> -->
                  </table>
               </div>
               <div role="tabpanel" class="tab-pane" id="tablaDatos">
                  <div>
                     <!-- Nav tabs -->
                     <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                           <a href="#ingreso" aria-controls="ingreso" role="tab" data-toggle="tab"><strong>Ingreso Patron/Qnal. </strong></a>
                        </li>
                        <li role="presentation">
                           <a href="#egreso" aria-controls="egreso" role="tab" data-toggle="tab"><strong>Pago de prestamos</strong></a>
                        </li>
                        <li role="presentation">
                           <a href="#bene" aria-controls="bene" role="tab" data-toggle="tab"><strong>Retiro parcial/Prestamos</strong></a>
                        </li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="ingreso">
                           <table id="table"  class="table table-striped table-bordered" width="100%" cellspacing="0">
                              <thead>
                                 <th>Fecha Nomina</th>
                                 <th>Monto</th>
                                 <th>Codigo de Concepto</th>
                                 <th>Nombre del Concepto</th>
                              </thead>
                              <tbody></tbody>
                           </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="egreso">
                           <table id="tabla1"  class="table table-striped table-bordered" width="100%" cellspacing="0">
                              <thead>
                                 <th>Fecha Nomina</th>
                                 <th>Monto</th>
                                 <th>Codigo de Concepto</th>
                                 <th>Nombre del Concepto</th>
                              </thead>
                              <tbody></tbody>
                           </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="bene">
                           <table id="tablaretpres"  class="table table-striped table-bordered" width="100%" cellspacing="0">
                              <thead>
                                 <th>fecha</th>
                                 <th>monto</th>
                                 <th>concepto</th>
                              </thead>
                              <tbody></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="settings">
               </div>
            </div>
         </div>
        <!--  <input type="button" name="crear" id="crear" value="crear"> --> 
      </div>

</div>
@endsection