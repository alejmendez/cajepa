@extends('admin::layouts.default')
@section('content')
	@include('admin::partials.botonera')

	{{ Form::bsModalBusqueda([
		'Nombre' => '40',
		'Nomina' => '30',
		'Codigo' => '30'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Codigo Condicional</span>
		</li>
	</ul>
	
	<div class="row">
		{!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
		
			{{ Form::bsText('nombre', '', [
				'label' => 'Nombre',
				'placeholder' => 'Nombre',
				'required' => 'required'
			]) }} 
			
			{{ Form::bsSelect('codigo_nomina_id', $controller->codigo_nominas(), '',[
				'label' => 'Codigo Nomina',
				'placeholder' => 'Codigo Nomina',
				'required' => 'required'
			]) }}
		
		{!! Form::close() !!}
	</div>
@endsection