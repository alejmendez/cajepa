@extends('admin::layouts.default')
@section('content')
	{{ Form::bsModalBusqueda([
		'Nombre' => '100'
	]) }}

	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="{{ url('/') }}">Inicio</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Cargar</span>
		</li>
	</ul>
<div class="row">
	<div class="col-md-6">
		<form action="{{Route('excel')}}" enctype="multipart/form-data" method="POST" files="true">		
			<input type="file" name="archivo">
			<button type="submit">
			subir</button>
			{{ csrf_field() }}
		</form>
	</div>
</div>
@if(Session::has("exito"))
Tu excel subio correctamente 

{{Session::forget("exito")}}
@endif
@endsection