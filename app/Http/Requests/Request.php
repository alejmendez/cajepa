<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class Request extends FormRequest
{
	protected $rules = [];

	public function __construct(Factory $factory){
		$factory->extend(
			'telefono',
			function ($attribute, $value, $parameters){
				 return preg_match("/^[0-9]{4}\-[0-9]{3}\-[0-9]{4}$/", $value);
			},
			\Lang::get('validation.telefono')
		);

		$factory->extend(
			'password',
			function ($attribute, $value, $parameters){
				 return preg_match("/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $value);
			},
			\Lang::get('validation.password')
		);

		$factory->extend(
			'nombre',
			function ($attribute, $value, $parameters){
				 return preg_match("/^[a-z\_ ñáéíóú]+$/i", $value);
			},
			\Lang::get('validation.nombre')
		);
	}
	/**
	 * En el caso de utilizar reglas de validacion al momento de actualizar un registro
	 * este proceso agregas las reglas de manera automatica para no tener error de validacion
	 * con los campos unicos, ejemplo:
	 *
	 * 'correo' => ['required', 'email', 'unique:app_usuario,correo'],
	 *
	*/
	public function reglas($rules = [])
	{
		$id = (int) $this->input('id', 0);

		if (empty($rules)){
			$rules = $this->rules;
		}

		if ($id !== 0){
			foreach ($rules as $key => $value){
				if (is_string($value)) continue;

				foreach ($value as $k => $v){
					if (preg_match("/^unique\:/i", $v)){
						$rules[$key][$k] .= "," . $id;
					}
				}
			}
		}

		return $rules;
	}

	public function rules() {
		//$rules = $this->rules;
		$rules = $this->reglas();

		switch ($this->method()) {
			case 'GET':
			case 'DELETE':
				return [];
			case 'POST':
			case 'PUT':
			case 'PATCH':
				break;
			default:
				break;
		}

		return $rules;
	}

	public function authorize() {
		return \Auth::check();
	}
}
