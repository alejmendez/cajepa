<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController {
	protected $titulo = 'Backend';
	public $prefijo_ruta = '';

	public $autenticar = true;

	public $usuario = false;

	protected $ruta = '';

	public $_js = [];
	public $js = [];
	protected $patch_js = [
		'public/js',
		'public/plugins',
	];

	public $_css = [];
	public $css = [];
	protected $patch_css = [
		'public/css',
		'public/plugins',
	];

	public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons', 
		'jquery-easing', 'jquery-migrate', 
		'animate', 'bootstrap', 'bootbox',
		//'jquery-cookie'
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify',
		'init'
	];

	protected $libreriaEntorno = '';

	public $librerias = [];

	protected $_librerias = [];

	public function __construct() {
		$this->session();
		$this->prefijo_ruta = \Config::get('admin.prefix');

		$this->libreriaEntorno = \Config::get('admin.libreriaEntorno');
		$this->_librerias = \Config::get('admin.librerias')[$this->libreriaEntorno];

		$agente = @$_SERVER['HTTP_USER_AGENT'];

		if (preg_match('/(?i)msie [5-8]/', $agente)){
			array_unshift($this->libreriasIniciales, 'jquery');
			array_unshift($this->libreriasIniciales, 'ie');
		}else{
			array_unshift($this->libreriasIniciales, 'jquery2');
		}

		if ($this->autenticar) {
			$this->middleware('auth');
			$this->middleware('permisologia');
		}
	}

	protected function session() {
		$this->usuario = \Auth::user();
		if (is_null(\Session::get('id_usuario')) && (!is_null($this->usuario))) {
			\Session::put('id_usuario', $this->usuario->id);
			\Session::put('nombre_usuario', $this->usuario->nombre);
		}
	}

	protected function ruta() {
		if ($this->ruta != '' && !is_null(\Route::current())) {
			return $this->ruta;
		}

		if (!is_null(\Route::current())){
			$this->ruta = \Route::current()->getUri();
			$this->ruta = trim(preg_replace('/\{\w+\?\}/i', '', $this->ruta), '/');
		}
		
		return $this->ruta;
	}

	protected function view($vista, Array $variables = []) {
		return view($vista, $this->_app($variables));
	}

	public function permisologia($ruta = '') {
		if ($ruta === '') {
			$ruta = $this->ruta();
		}

		$ruta = substr($ruta, strlen(\Config::get('admin.prefix')) + 1);

		if (is_null($this->usuario)) {
			return false;
		}

		if (strtolower($this->usuario->super) === 's') {
			return true;
		}

		$sql = \DB::table('app_perfiles_permisos')
			->leftJoin('app_perfil', 'app_perfil.id', '=', 'app_perfiles_permisos.app_perfil_id')
			->where('app_perfil.id', $this->usuario->app_perfil_id)
			->where('app_perfiles_permisos.ruta', $ruta);

		$permisologia = $sql->count() > 0;

		if (!$permisologia) {
			$sql = \DB::table('app_usuario_permisos')
				->leftJoin('app_usuario', 'app_usuario.id', '=', 'app_usuario_permisos.app_usuario_id')
				->where('app_usuario.id', $this->usuario->id)
				->where('app_usuario_permisos.ruta', $ruta);

			$permisologia = $sql->count() > 0;
		}

		return $permisologia;
	}

	public function _app(Array $arr = []) {
		return array_merge([
			'controller' => $this,
			'usuario' => \Auth::user(),
			'html' => [
				'titulo' => $this->titulo(),
				'js' => $this->_js(),
				'css' => $this->_css(),
			],
		], $arr);
	}

	public function titulo() {
		return $this->titulo;
	}

	protected function _archivos($ruta, $archivos, $ext) {
		$arr = [];
		
		$archivos[] = strlen($this->prefijo_ruta) > 0 ? 
			substr($this->ruta(), strlen($this->prefijo_ruta) + 1) :
			$this->ruta();

		if (!is_array($ruta)) {
			$ruta = [$ruta];
		}

		foreach ($archivos as $archivo) {
			if (filter_var($archivo, FILTER_VALIDATE_URL)) {
				$arr[] = $archivo;
				continue;
			}

			//if (!preg_match('/^.*\.(' . $ext . ')$/i', $archivo)) {
			if (!preg_match('/\.(' . $ext . ')$/i', $archivo)) {
				$archivo .= '.' . $ext;
			}

			for ($i = 0, $c = count($ruta); $i < $c; $i++) {
				if (is_file($ruta[$i] . '/' . $archivo)) {
					$arr[] = url($ruta[$i] . '/' . $archivo);
					break;
				}
			}
		}

		return array_unique($arr);
	}

	protected function librerias($tipo){
		$arr = [];

		$librerias = array_merge($this->libreriasIniciales, $this->librerias);

		foreach ($librerias as $libreria) {
			if (isset($this->_librerias[$libreria][$tipo])){
				$arr = array_merge($arr, $this->_librerias[$libreria][$tipo]);
			}
		}

		return $arr;
	}

	protected function _js() {
		$arr = array_merge($this->librerias('js'), $this->_js, $this->js);
		return $this->_archivos($this->patch_js, $arr, 'js');
	}

	protected function _css() {
		$arr = array_merge($this->librerias('css'), $this->_css, $this->css);
		return $this->_archivos($this->patch_css, $arr, 'css');
	}

	public function limit_text($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}
		return $text;
	}

	protected function slugify($text) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}