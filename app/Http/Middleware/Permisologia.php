<?php

namespace App\Http\Middleware;

use Closure;

class Permisologia {
	protected $usuario = false;
	protected $ruta = '';

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$this->usuario = \Auth::user();

		if (!$this->permisologia()) {
			if (\Request::ajax()) {
				return \Lang::get('backend.without_permission_ajax');
			} else {
				return view('admin::errors/permiso', [
					'controller' => $this,
					'usuario' => $this->usuario,
					'html' => [
						'titulo' => 'Error',
						'js' => [],
						'css' => [],
					],
				]);
			}
		}

		return $next($request);
	}

	protected function ruta() {
		if ($this->ruta != '' && !is_null(\Route::current())) {
			return $this->ruta;
		}

		$this->ruta = \Route::current()->getUri();
		$this->ruta = trim(preg_replace('/\{\w+\?\}/i', '', $this->ruta), '/');

		return $this->ruta;
	}

	protected function permisologia($ruta = '') {
		if ($ruta === '') {
			$ruta = $this->ruta();
		}

		if (strlen(\Config::get('admin.prefix')) > 0){
			$ruta = substr($ruta, strlen(\Config::get('admin.prefix')) + 1);
		}

		if (is_null($this->usuario)) {
			return false;
		}

		if (strtolower($this->usuario->super) === 's') {
			return true;
		}

		$sql = \DB::table('app_perfiles_permisos')
			->leftJoin('app_perfil', 'app_perfil.id', '=', 'app_perfiles_permisos.app_perfil_id')
			->where('app_perfil.id', $this->usuario->app_perfil_id)
			->where('app_perfiles_permisos.ruta', $ruta);

		$permisologia = $sql->count() > 0;

		if (!$permisologia) {
			$sql = \DB::table('app_usuario_permisos')
				->leftJoin('app_usuario', 'app_usuario.id', '=', 'app_usuario_permisos.app_usuario_id')
				->where('app_usuario.id', $this->usuario->id)
				->where('app_usuario_permisos.ruta', $ruta);

			$permisologia = $sql->count() > 0;
		}

		return $permisologia;
	}
}
